
class Utils {
  static isUpperCase(str: string) {
    return str === str.toUpperCase();
  }

  static isLowerCase(str: string) {
      return str === str.toLowerCase();
  }

  static capitalize(str: string) {
    const splits = str.split(" ");
    return splits.map((word: string, index: number) => word.substr(0, 1).toUpperCase() + word.substr(1).toLowerCase()).join(" ");
  }
}

//extend String definition with extra utility methods
String.prototype.isUpperCase = function(): boolean {
  return this.valueOf() === this.valueOf().toUpperCase();
};
declare global {
    interface String {
        isUpperCase() : boolean;
    }
}

String.prototype.isLowerCase = function(): boolean {
  return this.valueOf() === this.valueOf().toLowerCase();
};
declare global {
    interface String {
        isLowerCase() : boolean;
    }
}

String.prototype.capitalize = function(): string {
    const splits = this.split(" ");
    return splits.map((word: string, index: number) => word.substr(0, 1).toUpperCase() + word.substr(1).toLowerCase()).join();
};
declare global {
    interface String {
        capitalize() : string;
    }
}


export default Utils;