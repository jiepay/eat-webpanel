import * as React from 'react';
import {
  BrowserRouter as Router,
  Route,
  // Link,
  Switch,
  Redirect,
} from 'react-router-dom';
import axios from 'axios';
import ls from 'local-storage';
import Login from './components/Login';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as JioiActions from './actions';
import { LocaleProvider } from 'antd';
import fr_FR from 'antd/lib/locale-provider/fr_FR';
import en_US from 'antd/lib/locale-provider/en_US';
import AdminDashboard from './components/AdminDashboard';
// import EditApplication from './components/EditApplication';
import Actions from './components/Actions';
import LoadingScreen from './components/LoadingScreen';
// import Dashboard from './components/Dashboard';
import { appSetIsInitializing } from './actions';
// import CreateAdmin from './components/CreateAdmin';
// import CardDisplay from './components/CardDisplay';
import PageLayout from './components/PageLayout';
import PageNotFound from './components/PageNotFound';
// import Application from './components/Application';
// import EditAdmin from './components/EditAdmin';
// import ViewApplication from './components/ViewApplication';

interface Props{
  auth: any;
  app: any;
  store?: any;
}
interface State{}
class App extends React.Component<Props, State> {
  componentDidMount(){
    // console.log("App mounting");
    // //auth
    // const token = this.props.auth.token;
    // if(token && this.props.auth.user){
    //   axios.defaults.headers.common['Authorization'] = `Basic ${token}`;
    //   //get self user
    //   Actions.getSelfUser((success) => {
    //     this.props.store.dispatch(appSetIsInitializing(false));
    //   });
    // }
    // else{
    //   delete axios.defaults.headers.common['Authorization'];
    //   this.props.store.dispatch(appSetIsInitializing(false));
    // }
  }

  render() {
    const l = this.props.app.locale;
    //if page is still loading data, show loading spinner
    if(this.props.auth.isInitializing && this.props.auth.isFetching){
      return(
        <Router>
          <div className="App">
            <LocaleProvider locale={this.props.app.locale == "en" ? en_US : fr_FR}>
              <LoadingScreen isLoading={true} />
            </LocaleProvider>
          </div>
        </Router>
      );
    }
    //if fetch failed
    if(this.props.auth.isInitializing && this.props.auth.isError && this.props.auth.error.message === "Connection Error"){
      return(
        <Router>
          <div className="App">
            <LocaleProvider locale={this.props.app.locale == "en" ? en_US : fr_FR}>
              <LoadingScreen
                isLoading={false} 
                isError={true} 
                message="Connection Error"
                description="Please check your connection and reload this page"
                />
            </LocaleProvider>
          </div>
        </Router>
      );
    }
    return (
      <Router>
        <div className="App">
          <LocaleProvider locale={en_US}>
            <Switch>
              <Route exact path="/" render={(props: any) => (
                // this.props.auth.authenticated ?
                //if authenticated, take user to dashboard
                <Redirect to={{
                  pathname: '/orders',
                  state: { from: props.location }
                }}/>
                // :
                // //if not authenticated, take user to login page
                // <Redirect to={{
                //   pathname: '/login',
                //   state: { from: props.location }
                // }}/>
              )} />
              <Route path="/orders" render={(props: any) => (
                // this.props.auth.authenticated ?
                //if authenticated, take user to dashboard
                <AdminDashboard {...props} app={this.props.app} auth={this.props.auth} page="orders" />
                // :
                // //if not authenticated, take user to login page
                // <Redirect to={{
                //   pathname: '/login',
                //   state: { from: props.location }
                // }}/>
              )} />
              <Route path="/customers" render={(props: any) => (
                // this.props.auth.authenticated ?
                //if authenticated, take user to dashboard
                <AdminDashboard {...props} app={this.props.app} auth={this.props.auth} page="customers" />
                // :
                // //if not authenticated, take user to login page
                // <Redirect to={{
                //   pathname: '/login',
                //   state: { from: props.location }
                // }}/>
              )} />
              <Route path="/foodmenu" render={(props: any) => (
                // this.props.auth.authenticated ?
                //if authenticated, take user to dashboard
                <AdminDashboard {...props} app={this.props.app} auth={this.props.auth} page="foodmenu" />
                // :
                // //if not authenticated, take user to login page
                // <Redirect to={{
                //   pathname: '/login',
                //   state: { from: props.location }
                // }}/>
              )} />
              <Route path="/store" render={(props: any) => (
                // this.props.auth.authenticated ?
                //if authenticated, take user to dashboard
                <AdminDashboard {...props} app={this.props.app} auth={this.props.auth} page="store" />
                // :
                // //if not authenticated, take user to login page
                // <Redirect to={{
                //   pathname: '/login',
                //   state: { from: props.location }
                // }}/>
              )} />
              {/* <Route path="/admins/create" render={(props: any) => (
                  this.props.auth.authenticated && this.props.auth.user.role === 'S' ?
                  <CreateAdmin {...props} locale={this.props.app.locale} auth={this.props.auth} app={this.props.app} />
                :
                  <Redirect to={{
                    pathname: '/',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              {/* <Route path="/accreds/create" render={(props: any) => (
                  this.props.auth.authenticated ?
                  <Application {...props} auth={this.props.auth} app={this.props.app} />
                :
                  <Redirect to={{
                    pathname: '/',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              {/* <Route path="/accreds/:id/view" render={(props: any) => (
                  this.props.auth.authenticated ?
                  <ViewApplication {...props} app={this.props.app} auth={this.props.auth} />
                :
                  <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              {/* <Route path="/accreds/:id" render={(props: any) => (
                  this.props.auth.authenticated ?
                  <EditApplication {...props} app={this.props.app} auth={this.props.auth} />
                :
                  <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              {/* <Route path="/admins/:id" render={(props: any) => (
                  this.props.auth.authenticated ?
                  <EditAdmin {...props} app={this.props.app} auth={this.props.auth} />
                :
                  <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              {/* <Route path="/cards/:id" render={(props: any) => (
                  this.props.auth.authenticated ?
                  <CardDisplay {...props} app={this.props.app} auth={this.props.auth} />
                :
                  <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                  }}/>
              )} /> */}
              <Route path="/login" render={(props: any) => (
                // this.props.auth.authenticated == false ?
                  <Login {...props} locale={this.props.app.locale} auth={this.props.auth} />
                // :
                //   <Redirect to={{
                //     pathname: '/',
                //     state: { from: props.location }
                //   }}/>
              )} />
              <Route component={PageNotFound} />
            </Switch>
          </LocaleProvider>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    app: state.app,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    actions: bindActionCreators<any, any>(JioiActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
