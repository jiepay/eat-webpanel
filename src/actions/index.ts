// import { CALL_API } from "redux-api-middleware";
import { Moment } from "moment";
import axios from 'axios';


// const baseUrl = `http://localhost:8000`;
const baseUrl = `https://reovofood.herokuapp.com`;
const apiBaseUrl = `${baseUrl}/api`;

axios.defaults.baseURL = baseUrl;
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

//debug
axios.defaults.headers.common['Authorization'] = 'Token 27270aeb2b1e78e2eb9d49abc02e2e89824529a0';
// axios.defaults.headers.common['Origin'] = 'http://localhost:3000';


export interface LoginAction {
  type: '/login/';
  token: string;
}
export const LOGIN = '/login/';
export function login(token: string): LoginAction {
  return {
    type: LOGIN,
    token: token,
  };
}

export const LOGOUT = '/logout/'
export function logout(){
  return {
    type: LOGOUT,
  };
}

//JIOI
// export const CREATE_APPLICATION_REQUEST = '/application/create/REQUEST';
// export const CREATE_APPLICATION_SUCCESS = '/application/create/SUCCESS';
// export const CREATE_APPLICATION_FAILURE = '/application/create/FAILURE';
// export function createApplication(data: any, photoFile: File, crop: any) {
//   const body = new FormData();
//   body.append("photo", photoFile);
//   body.append("photo_crop_x", crop.x);
//   body.append("photo_crop_y", crop.y);
//   body.append("photo_crop_width", crop.width);
//   body.append("photo_crop_height", crop.height);
//   body.append("last_name", data.last_name);
//   body.append("name", data.name);
//   body.append("country", data.country.toLowerCase());
//   body.append("dob", data.dob);
//   body.append("gender", data.gender);
//   body.append("nationality", data.nationality.toLowerCase());
//   body.append("passport_no", data.passport_no);
//   body.append("passport_exp", data.passport_exp);
//   body.append("status", data.status);
//   body.append("discipline", data.discipline);
//   return {
//     [CALL_API]: {
//       endpoint: `${apiBaseUrl}/applications/`,
//       method: 'POST',
//       types: [CREATE_APPLICATION_REQUEST, CREATE_APPLICATION_SUCCESS, CREATE_APPLICATION_FAILURE],
//       body: body
//     }
//   }
// }

//App
export interface AppSetAccountTypeAction {
  type: 'App/SET_ACCOUNT_TYPE';
  accountType: "superadmin" | "admin" | "user";
}
export const APP_SET_ACCOUNT_TYPE = 'App/SET_ACCOUNT_TYPE';
export function appSetAccountType(accountType: "superadmin" | "admin" | "user"): AppSetAccountTypeAction {
  return {
    type: APP_SET_ACCOUNT_TYPE,
    accountType: accountType,
  };
}


export interface AppSetLocaleAction {
  type: 'App/SET_LOCALE';
  locale: string;
}
export const APP_SET_LOCALE = 'App/SET_LOCALE';
export function appSetLocale(locale: string): AppSetLocaleAction {
  return {
    type: APP_SET_LOCALE,
    locale: locale,
  };
}

export interface AppSetIsInitializingAction {
  type: 'App/SET_IS_INITIALIZING';
  isInitializing: boolean;
}
export const APP_SET_IS_INITIALIZING = 'App/SET_IS_INITIALIZING';
export function appSetIsInitializing(isInitializing: boolean): AppSetIsInitializingAction {
  return {
    type: APP_SET_IS_INITIALIZING,
    isInitializing: isInitializing,
  };
}

export interface BeginLoginAction {
  type: 'App/BEGIN_LOGIN';
}
export const BEGIN_LOGIN = 'App/BEGIN_LOGIN';
export function beginLogin(): BeginLoginAction {
  return {
    type: BEGIN_LOGIN,
  };
}

export interface FailLoginAction {
  type: 'App/FAIL_LOGIN';
  error: string;
}
export const FAIL_LOGIN = 'App/FAIL_LOGIN';
export function failLogin(error: string): FailLoginAction {
  return {
    type: FAIL_LOGIN,
    error: error,
  };
}

export interface AppLoginAction {
  type: 'App/LOGIN';
  token: string;
  user: any;
}
export const APP_LOGIN = 'App/LOGIN';
export function appLogin(token: string, user: any): AppLoginAction {
  return {
    type: APP_LOGIN,
    token: token,
    user: user,
  };
}

export interface BeginGetSelfUserAction {
  type: 'App/BEGIN_GET_SELF_USER';
}
export const BEGIN_GET_SELF_USER = 'App/BEGIN_GET_SELF_USER';
export function beginGetSelfUser(): BeginGetSelfUserAction {
  return {
    type: BEGIN_GET_SELF_USER,
  };
}

export interface FailGetSelfUserAction {
  type: 'App/FAIL_GET_SELF_USER';
  error: string;
}
export const FAIL_GET_SELF_USER = 'App/FAIL_GET_SELF_USER';
export function failGetSelfUser(error: string): FailGetSelfUserAction {
  return {
    type: FAIL_GET_SELF_USER,
    error: error,
  };
}

export interface AppSetSelfUserAction {
  type: 'App/SET_SELF_USER';
  user: any;
}
export const APP_SET_SELF_USER = 'App/SET_SELF_USER';
export function appSetSelfUser(user: any): AppSetSelfUserAction {
  return {
    type: APP_SET_SELF_USER,
    user: user,
  };
}

export interface BeginCreateApplicationAction {
  type: 'App/BEGIN_CREATE_APPLICATION';
}
export const BEGIN_CREATE_APPLICATION = 'App/BEGIN_CREATE_APPLICATION';
export function beginCreateApplication(): BeginCreateApplicationAction {
  return {
    type: BEGIN_CREATE_APPLICATION,
  };
}

export interface FailCreateApplicationAction {
  type: 'App/FAIL_CREATE_APPLICATION';
  error: string;
}
export const FAIL_CREATE_APPLICATION = 'App/FAIL_CREATE_APPLICATION';
export function failCreateApplication(error: string): FailCreateApplicationAction {
  return {
    type: FAIL_CREATE_APPLICATION,
    error: error,
  };
}

export interface AppCreateApplicationAction {
  type: 'App/CREATE_APPLICATION';
  application: any;
}
export const APP_CREATE_APPLICATION = 'App/CREATE_APPLICATION';
export function appCreateApplication(application: any): AppCreateApplicationAction {
  return {
    type: APP_CREATE_APPLICATION,
    application: application,
  };
}

export interface BeginCreateAdminAction {
  type: 'App/BEGIN_CREATE_ADMIN';
}
export const BEGIN_CREATE_ADMIN = 'App/BEGIN_CREATE_ADMIN';
export function beginCreateAdmin(): BeginCreateAdminAction {
  return {
    type: BEGIN_CREATE_ADMIN,
  };
}

export interface FailCreateAdminAction {
  type: 'App/FAIL_CREATE_ADMIN';
  error: string;
}
export const FAIL_CREATE_ADMIN = 'App/FAIL_CREATE_ADMIN';
export function failCreateAdmin(error: string): FailCreateAdminAction {
  return {
    type: FAIL_CREATE_ADMIN,
    error: error,
  };
}

export interface AppCreateAdminAction {
  type: 'App/CREATE_ADMIN';
  user: any;
}
export const APP_CREATE_ADMIN = 'App/CREATE_ADMIN';
export function appCreateAdmin(user: any): AppCreateAdminAction {
  return {
    type: APP_CREATE_ADMIN,
    user: user,
  };
}

export interface BeginGetCustomersAction {
  type: 'BEGIN_GET_CUSTOMERS';
}
export const BEGIN_GET_CUSTOMERS = 'BEGIN_GET_CUSTOMERS';
export function beginGetCustomers(): BeginGetCustomersAction {
  return {
    type: BEGIN_GET_CUSTOMERS,
  };
}

export interface FailGetCustomersAction {
  type: 'FAIL_GET_CUSTOMERS';
  error: string;
}
export const FAIL_GET_CUSTOMERS = 'FAIL_GET_CUSTOMERS';
export function failGetCustomers(error: string): FailGetCustomersAction {
  return {
    type: FAIL_GET_CUSTOMERS,
    error: error,
  };
}

export interface AppSetCustomersAction {
  type: 'SET_CUSTOMERS';
  customers: any[];
}
export const APP_SET_CUSTOMERS = 'SET_CUSTOMERS';
export function appSetCustomers(customers: any[]): AppSetCustomersAction {
  return {
    type: APP_SET_CUSTOMERS,
    customers: customers,
  };
}

export interface BeginGetCategoriesAction {
  type: 'App/BEGIN_GET_CATEGORIES';
}
export const BEGIN_GET_CATEGORIES = 'App/BEGIN_GET_CATEGORIES';
export function beginGetCategories(): BeginGetCategoriesAction {
  return {
    type: BEGIN_GET_CATEGORIES,
  };
}

export interface FailGetCategoriesAction {
  type: 'App/FAIL_GET_CATEGORIES';
  error: string;
}
export const FAIL_GET_CATEGORIES = 'App/FAIL_GET_CATEGORIES';
export function failGetCategories(error: string): FailGetCategoriesAction {
  return {
    type: FAIL_GET_CATEGORIES,
    error: error,
  };
}

export interface AppSetCategoriesAction {
  type: 'App/SET_CATEGORIES';
  categories: any[];
}
export const APP_SET_CATEGORIES = 'App/SET_CATEGORIES';
export function appSetCategories(categories: any[]): AppSetCategoriesAction {
  return {
    type: APP_SET_CATEGORIES,
    categories: categories,
  };
}

export interface BeginUpdateOrderDeliveredAction {
  type: 'App/BEGIN_UPDATE_ORDER_DELIVERED';
  orderId: number;
}
export const BEGIN_UPDATE_ORDER_DELIVERED = 'App/BEGIN_UPDATE_ORDER_DELIVERED';
export function beginUpdateOrderDelivered(orderId: number): BeginUpdateOrderDeliveredAction {
  return {
    type: BEGIN_UPDATE_ORDER_DELIVERED,
    orderId: orderId,
  };
}

export interface FailUpdateOrderDeliveredAction {
  type: 'App/FAIL_UPDATE_ORDER_DELIVERED';
  error: string;
}
export const FAIL_UPDATE_ORDER_DELIVERED = 'App/FAIL_UPDATE_ORDER_DELIVERED';
export function failUpdateOrderDelivered(error: string): FailUpdateOrderDeliveredAction {
  return {
    type: FAIL_UPDATE_ORDER_DELIVERED,
    error: error,
  };
}

export interface AppUpdateOrderDeliveredAction {
  type: 'App/APP_SET_UPDATE_ORDER_DELIVERED';
}
export const APP_SET_UPDATE_ORDER_DELIVERED = 'App/APP_SET_UPDATE_ORDER_DELIVERED';
export function appUpdateOrderDelivered(): AppUpdateOrderDeliveredAction {
  return {
    type: APP_SET_UPDATE_ORDER_DELIVERED,
  };
}

export interface BeginUpdateStoreDetailsAction {
  type: 'App/BEGIN_UPDATE_STORE_DETAILS';
}
export const BEGIN_UPDATE_STORE_DETAILS = 'App/BEGIN_UPDATE_STORE_DETAILS';
export function beginUpdateStoreDetails(): BeginUpdateStoreDetailsAction {
  return {
    type: BEGIN_UPDATE_STORE_DETAILS,
  };
}

export interface FailUpdateStoreDetailsAction {
  type: 'App/FAIL_UPDATE_STORE_DETAILS';
  error: string;
}
export const FAIL_UPDATE_STORE_DETAILS = 'App/FAIL_UPDATE_STORE_DETAILS';
export function failUpdateStoreDetails(error: string): FailUpdateStoreDetailsAction {
  return {
    type: FAIL_UPDATE_STORE_DETAILS,
    error: error,
  };
}

export interface AppUpdateStoreDetailsAction {
  type: 'App/APP_SET_UPDATE_STORE_DETAILS';
  shops: any[];
}
export const APP_SET_UPDATE_STORE_DETAILS = 'App/APP_SET_UPDATE_STORE_DETAILS';
export function appUpdateStoreDetails(shops: any[]): AppUpdateStoreDetailsAction {
  return {
    type: APP_SET_UPDATE_STORE_DETAILS,
    shops: shops,
  };
}

export interface BeginUpdateOrderCancelledAction {
  type: 'App/BEGIN_UPDATE_ORDER_CANCELLED';
  orderId: number;
}
export const BEGIN_UPDATE_ORDER_CANCELLED = 'App/BEGIN_UPDATE_ORDER_CANCELLED';
export function beginUpdateOrderCancelled(orderId: number): BeginUpdateOrderCancelledAction {
  return {
    type: BEGIN_UPDATE_ORDER_CANCELLED,
    orderId: orderId,
  };
}

export interface FailUpdateOrderCancelledAction {
  type: 'App/FAIL_UPDATE_ORDER_CANCELLED';
  error: string;
}
export const FAIL_UPDATE_ORDER_CANCELLED = 'App/FAIL_UPDATE_ORDER_CANCELLED';
export function failUpdateOrderCancelled(error: string): FailUpdateOrderCancelledAction {
  return {
    type: FAIL_UPDATE_ORDER_CANCELLED,
    error: error,
  };
}

export interface AppUpdateOrderCancelledAction {
  type: 'App/APP_SET_UPDATE_ORDER_CANCELLED';
}
export const APP_SET_UPDATE_ORDER_CANCELLED = 'App/APP_SET_UPDATE_ORDER_CANCELLED';
export function appUpdateOrderCancelled(): AppUpdateOrderCancelledAction {
  return {
    type: APP_SET_UPDATE_ORDER_CANCELLED,
  };
}



export interface BeginGetShopsAction {
  type: 'App/BEGIN_GET_SHOPS';
}
export const BEGIN_GET_SHOPS = 'App/BEGIN_GET_SHOPS';
export function beginGetShops(): BeginGetShopsAction {
  return {
    type: BEGIN_GET_SHOPS,
  };
}

export interface FailGetShopsAction {
  type: 'App/FAIL_GET_SHOPS';
  error: string;
}
export const FAIL_GET_SHOPS = 'App/FAIL_GET_SHOPS';
export function failGetShops(error: string): FailGetShopsAction {
  return {
    type: FAIL_GET_SHOPS,
    error: error,
  };
}

export interface AppSetShopsAction {
  type: 'App/SET_SHOPS';
  shops: any[];
}
export const APP_SET_SHOPS = 'App/SET_SHOPS';
export function appSetShops(shops: any[]): AppSetShopsAction {
  return {
    type: APP_SET_SHOPS,
    shops: shops,
  };
}

export interface BeginGetFoodsAction {
  type: 'App/BEGIN_GET_FOODS';
}
export const BEGIN_GET_FOODS = 'App/BEGIN_GET_FOODS';
export function beginGetFoods(): BeginGetFoodsAction {
  return {
    type: BEGIN_GET_FOODS,
  };
}

export interface FailGetFoodsAction {
  type: 'App/FAIL_GET_FOODS';
  error: string;
}
export const FAIL_GET_FOODS = 'App/FAIL_GET_FOODS';
export function failGetFoods(error: string): FailGetFoodsAction {
  return {
    type: FAIL_GET_FOODS,
    error: error,
  };
}

export interface AppSetFoodsAction {
  type: 'App/SET_FOODS';
  foods: any[];
}
export const APP_SET_FOODS = 'App/SET_FOODS';
export function appSetFoods(foods: any[]): AppSetFoodsAction {
  return {
    type: APP_SET_FOODS,
    foods: foods,
  };
}

export interface BeginGetOrdersAction {
  type: 'App/BEGIN_GET_ORDERS';
}
export const BEGIN_GET_ORDERS = 'App/BEGIN_GET_ORDERS';
export function beginGetOrders(): BeginGetOrdersAction {
  return {
    type: BEGIN_GET_ORDERS,
  };
}

export interface FailGetOrdersAction {
  type: 'App/FAIL_GET_ORDERS';
  error: string;
}
export const FAIL_GET_ORDERS = 'App/FAIL_GET_ORDERS';
export function failGetOrders(error: string): FailGetOrdersAction {
  return {
    type: FAIL_GET_ORDERS,
    error: error,
  };
}

export interface AppSetOrdersAction {
  type: 'App/SET_ORDERS';
  orders: any[];
}
export const APP_SET_ORDERS = 'App/SET_ORDERS';
export function appSetOrders(orders: any[]): AppSetOrdersAction {
  return {
    type: APP_SET_ORDERS,
    orders: orders,
  };
}

