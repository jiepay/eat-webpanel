import * as React from 'react';
import { Icon, Button,
  Form, Input, Checkbox, Row, Alert } from 'antd';
import {FormComponentProps} from 'antd/lib/form/Form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ls from 'local-storage';
import Actions from './Actions';
import axios from 'axios';

const FormItem = Form.Item;

interface Props extends FormComponentProps{
  locale?: string;
  dispatch?: any;
  auth?: any;
}
interface State{
  hasTried: boolean;
}

class LoginForm extends React.Component<Props, State> {
  constructor(props: FormComponentProps){
    super(props);
    this.state = {hasTried: false};
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({hasTried: true});
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if(process.env.NODE_ENV === "development"){
          console.log('Received values of form: ', values);
        }
        const email = (values.email as string).trim().toLowerCase();
        Actions.login(email, values.password);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const itemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
    let errorMsg = "";
    if(this.state.hasTried && this.props.auth.isError){
      if(!this.props.auth.error.response && this.props.auth.error.request){
        errorMsg = "Connection error";
      }
      else if(this.props.auth.error.response.status === 400 || this.props.auth.error.response.status === 401){
        errorMsg = "Incorrect email or password";
      }
      else if(this.props.auth.error.response.status === 500){
        errorMsg = "Server error";
      }
      else{
        errorMsg = "Unknown error";
      }
    }
    const error = (
      <Row>
        <Alert message={errorMsg} type="error" showIcon />
        <div style={{height: 30}}></div>
      </Row>
    );
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        {this.state.hasTried && this.props.auth.isError && error}
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              size="large"
              placeholder={"Email"} />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              size="large"
              placeholder={"Password"} />
          )}
        </FormItem>
        <FormItem>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Remember me</Checkbox>
          )}
          <a className="login-form-forgot" href="">Forgot password</a> */}
          <Row type="flex" justify="end">
              <Button type="primary" htmlType="submit" loading={this.props.auth.isFetching} className="login-form-button">
                Login
              </Button>
          </Row>
        </FormItem>
      </Form>
    );
  }
}

export default connect()(Form.create()(LoginForm));
