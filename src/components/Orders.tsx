import * as React from 'react';
import { Layout, Row, Col, Alert, Tabs } from 'antd';
import { connect } from 'react-redux';
import OrdersTable from './OrdersTable';
import PizzaLoader from './PizzaLoader';
import Actions from './Actions';
import ReconnectingWebSocket from 'reconnecting-websocket';
import { appSetOrders } from 'src/actions';

interface Props {
  orders: any;
  customers: any;
  dispatch?: any;
}
interface State { }

class Orders extends React.Component<Props, State> {
  orderWS;

  componentDidMount() {
    Actions.getCustomers();
    Actions.getOrders((success) => {
      //setup websocket connection
      // const ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
      // const ws = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host + "/ws/orders/");
      // this.orderWS = new ReconnectingWebSocket("ws://localhost:8000/ws/orders/");
      this.orderWS = new ReconnectingWebSocket("wss://reovofood.herokuapp.com/ws/orders/");
      this.orderWS.onopen = () => {
        console.log("Connection to orders/ open");
      };
      this.orderWS.onmessage = msg => {
        console.log("Message from orders/ received!");
        console.log("msg::: ", msg);
        const data = JSON.parse(msg.data);
        console.log("msg.data::: ", data);
        if (data.action === "create") {
          //new order was just created
          this.props.dispatch(appSetOrders([...this.props.orders.orders, data.object]));
        }
        else if (data.action === "update") {
          //order was just updated
          const list = [...this.props.orders.orders]; //copy current list of orders
          const order = list.filter(f => f.id === data.object.id)[0];
          if (order) {
            list[list.indexOf(order)] = data.object;
            this.props.dispatch(appSetOrders(list));
          }
          else {
            //if order is undefined, just refetch the whole list of orders
            Actions.getOrders();
          }
        }
        else if (data.action === "delete") {
          //order was just deleted
          const list = [...this.props.orders.orders]; //copy current list of orders
          const order = list.filter(f => f.id === data.id)[0];
          if (order) {
            list.splice(list.indexOf(order));
            this.props.dispatch(appSetOrders(list));
          }
          else {
            //if order is undefined, just refetch the whole list of orders
            Actions.getOrders();
          }
        }
      };
    });
  }
  render() {
    let content;
    //if page is still loading data, show loading spinner
    if (this.props.orders.isFetching || this.props.customers.isFetching) {
      content = (
        <Row type="flex" justify="center" align="middle" style={{ height: '40vh' }}>
          {/* <Spin size="large" /> */}
          <PizzaLoader />
        </Row>
      );
    }
    //if fetch failed
    else if (this.props.orders.isError || this.props.customers.isError) {
      content = (
        <Row type="flex" justify="center" align="middle" style={{ height: '40vh' }}>
          <Col md={12}>
            <Alert
              message="Connection Error"
              description="Please check your internet connection and reload this page."
              type="error"
              showIcon
            />
          </Col>
        </Row>
      );
    }
    else {
      //we have data
      content = (
        <Tabs defaultActiveKey="0" size="large">
          <Tabs.TabPane key="0" tab="Pending">
            <OrdersTable
              type="pending"
              orders={this.props.orders.orders.filter(o => o.status === "PENDING")}
              customers={this.props.customers.customers} />
          </Tabs.TabPane>
          <Tabs.TabPane key="1" tab="Delivered">
            <OrdersTable
              type="delivered"
              orders={this.props.orders.orders.filter(o => o.status === "DELIVERED")}
              customers={this.props.customers.customers} />
          </Tabs.TabPane>
          <Tabs.TabPane key="2" tab="Cancelled">
            <OrdersTable
              type="cancelled"
              orders={this.props.orders.orders.filter(o => o.status === "CANCELLED")}
              customers={this.props.customers.customers} />
          </Tabs.TabPane>
        </Tabs>
      );
    }
    return (
      <>
      <h1 style={{ textAlign: 'center', fontSize: '2em', marginBottom: 40 }}>Orders</h1>
      {content}
      </>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    orders: state.app.orders,
    customers: state.app.customers,
  };
}
export default connect(mapStateToProps)(Orders);