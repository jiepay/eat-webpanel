import * as React from 'react';
import { Layout } from 'antd';
import { Form, Input, InputNumber, Select, Row, Col, DatePicker, TimePicker, Button, Upload, message, Icon} from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { connect } from 'react-redux';

const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const { TextArea } = Input;

const imgpie = require("../res/img/pie.jpg")

interface Props extends FormComponentProps{}
interface State{}



class EditFood extends React.Component<Props, State> {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  handleChange(value) {
    console.log(`selected ${value}`);
  }


  render() {

    const { getFieldDecorator } = this.props.form;
    const config = {
      rules: [{ type: 'object', required: true, message: 'Please select time!' }],
    };

    const rangeConfig = {
      rules: [{ type: 'array', required: true, message: 'Please select time!' }],
    };
   
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <>
        <Form onSubmit={this.handleSubmit}>

        <FormItem>

            <img src={imgpie} style={{width: '100%'}} />
            <Upload {...this.props}>
                <Button>
                    <Icon type="upload" /> Click to Upload
                </Button>
            </Upload>
        
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Item Title"
        >
          {getFieldDecorator('title', {
            rules: [{
              required: true, message: 'Please input the title!',
            }],
          })(
            <Input placeholder="Title"/>
          )}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Description"
        >
          {getFieldDecorator('description', {
            rules: [{
              required: true, message: 'Please input the description!',
            }],
          })(
            <TextArea rows={4} placeholder="Description" />
          )}
        </FormItem>

          <FormItem
          {...formItemLayout}
          label="Base Price"
        >
          {getFieldDecorator('base price', {
            rules: [{
              required: true, message: 'Please input the price!',
            }],
          })(
            <Input placeholder="Price"/>
          )}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Discounted Price"
        >
          {getFieldDecorator('discounted price', {
            rules: [{
            }],
          })(
            <Input placeholder="Discounted Price"/>
          )}
        </FormItem>
          
        <FormItem
          {...formItemLayout}
          label="Discount Date"
        >
          {getFieldDecorator('range-time-picker', rangeConfig)(
            <RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />
          )}

        </FormItem>
        

        </Form>
  
      </>
    );
  }
}
export default connect()(Form.create()(EditFood));