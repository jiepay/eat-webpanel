import * as React from 'react';
import {Button, Layout, Menu, Icon, Row, Col, Avatar, Dropdown, Radio} from 'antd';
import { connect } from 'react-redux';
import {logout, appSetLocale} from '../actions';
import { Moment } from 'moment';
import * as moment from 'moment';
import 'moment/locale/zh-cn';
import fr_FR from 'antd/lib/locale-provider/fr_FR';
import {Link} from 'react-router-dom';


moment.locale('en');
const logo = require('../logo.svg');

const { Header, Sider, Content, Footer } = Layout;

interface Props{
  dispatch?: any;
  authenticated: boolean;
  locale: string;
}
interface State{
  width: number;
  height: number;
}

class NavBar extends React.Component<Props, State> {
  state = { width: 0, height: 0 };
  logout = () => {
    this.props.dispatch(logout());
  }
  changeLocale = (e) => {
    this.props.dispatch(appSetLocale(e.target.value));
    if (e.target.value != "fr_FR") {
      moment.locale('en');
    } else {
      moment.locale('fr_FR');
    }
    // window.location.reload();
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  render() {
    const avatarMenu = (
      <Menu>
        {/* <Menu.Item key="0">
          <a href="#">Profile</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="#">Settings</a>
        </Menu.Item>
        <Menu.Divider /> */}
        <Menu.Item key="3"><a href="#" onClick={this.logout}>Logout</a></Menu.Item>
      </Menu>
    );
    return (
      <Header style={{backgroundColor: '#fff', padding: 0, borderBottom: '1px solid #ddd'}}>
        <Row type="flex" justify="space-between" align="middle">
          <Col md={4} sm={24} style={{paddingLeft: 20}}>
            <Link to="/">
              <img src={logo} style={{height: 64}} />
            </Link>
          </Col>
          <Col md={12} sm={24} className="full-width-on-small header-text-container">
            <Row type="flex" justify="space-around" align="middle">
              <Col sm={24} style={{textAlign: 'center', height: 78, overflow: 'hidden'}}>
                <h1 className="header-text" style={{textAlign: 'center'}}>
                  Eat Food Hungry
                </h1>
              </Col>
            </Row>
          </Col>
          <Col md={6}>
            <Row type="flex" justify="end">
              {/* <Col className="lang-menu" md={20} sm={20} style={{left: this.state.width < 768 ? 'calc(100vw - 150px)' : 0}}>
                <Radio.Group value={this.props.locale} onChange={this.changeLocale} size={this.state.width < 768 ? "small" : "default" }>
                  <Radio.Button key="en" value="en">English</Radio.Button>
                  <Radio.Button key="fr_FR" value="fr_FR">Français</Radio.Button>
                </Radio.Group>
              </Col> */}
              {
                this.props.authenticated &&
                <Col md={4} sm={4}
                  style={{
                    left: this.state.width < 768 ? 'calc(100vw - 200px)' : 0,
                    top: this.state.width < 768 ? -145 : 0,
                  }}>
                  <Dropdown overlay={avatarMenu} trigger={['click']}>
                    <a className="ant-dropdown-link" href="#">
                      <Avatar size="large" icon="user" style={{ backgroundColor: '#666' }} />
                    </a>
                  </Dropdown>
                </Col>
              }
            </Row>
          </Col>
        </Row>
      </Header>
    );
  }
}

export default connect()(NavBar);