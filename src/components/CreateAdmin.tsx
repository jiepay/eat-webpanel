// import * as React from 'react';
// import { Row, Col, Card, Upload, Button, Icon, Table, Spin, Tooltip, } from 'antd';
// import { Link } from 'react-router-dom';
// import * as XLSX from 'xlsx';
// import PageLayout from './PageLayout';
// import CreateAdminForm from './CreateAdminForm';
// import Actions from './Actions';

// interface Props{
//   locale: string;
//   auth: any;
//   app: any;
// }
// interface State{
//   batch: any[];
//   isLoading: boolean;
// }

// export default class CreateAdmin extends React.Component<Props, State> {
//   state = {batch: [] as any[], isLoading: false}
//   batchProcessApplications = (file) => {
    
//   }
//   beforeUpload = (file) => {
//     // this.setState({photoFile: file, isImageDirty: true});
//     // console.log(file);
//     // this.batchProcessApplications(file);
//     return false;
//   }
//   onFileChange = (info) => {
//     const rABS = true; // true: readAsBinaryString ; false: readAsArrayBuffer
//     const reader = new FileReader();
//     reader.onload = (e) => {
//       let data = reader.result;
//       if(!rABS){
//         data = new Uint8Array(data);
//       }
//       const workbook = XLSX.read(data, {type: rABS ? 'binary' : 'array'});

//       /* DO SOMETHING WITH workbook HERE */
//       workbook.SheetNames.forEach((name, index) => {
//         console.log(name);
//         const admins = XLSX.utils.sheet_to_json(workbook.Sheets[name]);
//         console.log(admins);
//         this.setState({batch: []});
//         admins.forEach((admin: any, index) => {
//           const values = {
//             first_name: admin["First Name"],
//             last_name: admin["Last Name"],
//             email: admin["Email"],
//             password: admin["Password"],
//             country: admin["Country"],
//             status: null,
//             key: index,
//           };
//           this.setState({batch: [...this.state.batch, values]});
//         });
//       });
//     };
//     if(rABS){
//       reader.readAsBinaryString(info.file);
//     }
//     else{
//       reader.readAsArrayBuffer(info.file);
//     }
//   }
//   create = (index) => {
//     const data: any[] = [...this.state.batch];
//     const i = {...this.state.batch[index]};
//     i.status = "pending";
//     data[index] = {...this.state.batch[index]};
//     this.setState({batch: data, isLoading: true});
//     Actions.createAdmin(this.state.batch[index], (success) => {
//       i.status = success ? "success" : "error";
//       data[index] = i;
//       this.setState({batch: data});
//       this.updateLoading();
//     });
//   }
//   batchCreate = () => {
//     this.state.batch.forEach((item: any, index) => {
//       this.create(index);
//       console.log(index);
//     })
//   }
//   updateLoading = () => {
//     let result = false;
//     this.state.batch.forEach((item) => {
//       if(item.status === "pending"){
//         result = true;
//       }
//     });
//     this.setState({isLoading: result});
//   }
//   render() {
//     return (
//       <PageLayout>
//         <Row type="flex" justify="space-around" align="top" style={{flex: '1 1 auto'}}>
//           <Col md={16}>
//             {/* <Card hoverable={false} title={<Row><h3 style={{textAlign: 'center'}}>Batch Create Admins</h3></Row>}>
//               <p>You can create admins in batch from a spreadsheet. It must have the following column names: 
//                 First Name, Last Name, Email, Password, Country. Country should be provided in lowercase ISO alpha 2. 
//                 For example, "mu" for Mauritius. Supported countries are km, mg, mu, mv, re, sc and yt.</p>
//               <Upload beforeUpload={this.beforeUpload} onChange={this.onFileChange}>
//                 <Tooltip title="Upload a spreadsheet containing a list of admins">
//                   <Button type="primary">
//                     <Icon type="upload" /> Click to Upload
//                   </Button>
//                 </Tooltip>
//               </Upload>
//             {this.state.batch.length > 0 &&
//               <>
//                 <h4 style={{textAlign: 'center'}}>Preview</h4>
//                 <Table dataSource={this.state.batch} >
//                   <Table.Column
//                     title="First Name"
//                     dataIndex="first_name"
//                     key="first_name"
//                     />
//                   <Table.Column
//                     title="Last Name"
//                     dataIndex="last_name"
//                     key="last_name"
//                     />
//                   <Table.Column
//                     title="Email"
//                     dataIndex="email"
//                     key="email"
//                     />
//                   <Table.Column
//                     title="Password"
//                     dataIndex="password"
//                     key="password"
//                     />
//                   <Table.Column
//                     title="Country"
//                     dataIndex="country"
//                     key="country"
//                     />
//                   <Table.Column
//                     title="Status"
//                     dataIndex="status"
//                     key="status"
//                     render={(text, record: any, index)=>{
//                       if(record.status === "pending"){
//                         return <Spin size="small" />
//                       }
//                       else if(record.status === "success"){
//                         return <Icon type="check"/>
//                       }
//                       else if(record.status === "error"){
//                         return <Button onClick={()=>this.create(index)}>Retry</Button>
//                       }
//                       return <></> //return nothing if it's null
//                     }}
//                     />
//                 </Table>
//                 <Row type="flex" justify="center">
//                   <Button
//                     size="large"
//                     className="default-btn"
//                     style={{
//                       marginTop: 20,
//                       borderRadius: 20,
//                     }}
//                     loading={this.state.isLoading}
//                     disabled={this.state.isLoading}
//                     onClick={this.batchCreate}>
//                     Create Admins
//                   </Button>
//                 </Row>
//               </>
//             }
//             </Card>
//             <div style={{height: 30}}></div> */}
//             <Card hoverable={false} title={<Row><h3 style={{textAlign: 'center'}}>{translations(this.props.app.locale).createAdmin}</h3></Row>}>
//               <Row type="flex">
//                 <Col md={4}>
//                   <Link to="/">
//                     <Button icon="left">Go Back</Button>
//                   </Link>
//                 </Col>
//               </Row>
//               <CreateAdminForm locale={this.props.app.locale} auth={this.props.auth} app={this.props.app} />
//             </Card>
//           </Col>
//         </Row>
//       </PageLayout>
//     );
//   }
// }
