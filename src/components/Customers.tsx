import * as React from 'react';
import { Layout, Row, Col, Alert, Spin, Input, Button, Icon } from 'antd';
import {connect} from 'react-redux';
import CustomersTable from './CustomersTable';
import Actions from './Actions';
import PizzaLoader from './PizzaLoader';

interface Props{
  customers: any;
}
interface State{
  count: number;
  isCounting: boolean;
}

class Customers extends React.Component<Props, State> {
  state = {
    count: 0,
    isCounting: false,
  }
  componentDidMount() {
    Actions.getCustomers();
  }
  render() {
    let content;
    //if page is still loading data, show loading spinner
    if(this.props.customers.isFetching){
      content = (
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
            {/* <Spin size="large" /> */}
            <PizzaLoader />
        </Row>
      );
    }
    //if fetch failed
    else if(this.props.customers.isError){
      content = (
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
          <Col md={12}>
            <Alert
              message="Connection Error"
              description="Please check your internet connection and reload this page."
              type="error"
              showIcon
            />
          </Col>
        </Row>
      );
    }
    else{
      //we have customers
      content = (
        <>
          <Row type="flex" justify="center" style={{marginBottom: 30}}>
            <Col md={12}>
              <Input.Search
                placeholder="Broadcast message..."
                onSearch={msg => console.log(msg)}
                maxLength={100}
                onChange={e => {
                  this.setState({count: e.target.value.length});
                }}
                onFocus={e => {
                  this.setState({isCounting: true});
                }}
                onBlur={e => {
                  this.setState({isCounting: false});
                }}
                style={{width: '100%'}}
                enterButton={
                  <Button type="primary">
                    <Icon type="notification" style={{transform: "scale(-1, 1)"}} />
                  </Button>}
              />
              {
                this.state.isCounting &&
                <Row type="flex" justify="end">
                  {this.state.count.toString()}/100
                </Row>
              }
            </Col>
          </Row>
          <CustomersTable customers={this.props.customers.customers} />
        </>
      );
    }
    
    return (
      <>
        <h1 style={{textAlign: 'center', fontSize: '2em', marginBottom: 40}}>Customers</h1>
        {content}
      </>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    customers: state.app.customers,
  };
}
export default connect(mapStateToProps)(Customers);