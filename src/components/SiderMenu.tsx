import * as React from 'react';
import {Layout, Menu, Icon, Row, Col,} from 'antd';
import NavBar from './NavBar';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';


interface Props{
  collapsed: boolean;
  page: string;
}
interface State{}

class SiderMenu extends React.Component<Props, State> {
  render() {
    let selectedPage = "1";
    if(this.props.page === "customers"){
      selectedPage = "2";
    }
    else if(this.props.page === "foodmenu"){
      selectedPage = "3";
    }
    else if(this.props.page === "store"){
      selectedPage = "4";
    }
    return (
      <Menu theme="dark" mode="inline" selectedKeys={[selectedPage]}>
        <Menu.Item key="1">
          <Link to="/orders">
            <Icon type="profile" />
            <span>Orders</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/customers">
            <Icon type="team" />
            <span>Customers</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/foodmenu">
            <Icon type="book" />
            <span>Food Menu</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/store">
            <Icon type="setting" />
            <span>Store Details</span>
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    app: state.app,
  };
}

export default connect(mapStateToProps)(SiderMenu);