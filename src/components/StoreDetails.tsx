import * as React from 'react';
import { Layout, Spin, Alert } from 'antd';
import OrdersTable from 'src/components/OrdersTable';
import { Form, Input, InputNumber, Select, Row, Col, DatePicker, TimePicker, Button } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { connect } from 'react-redux';
import Actions from './Actions';
import * as moment from 'moment';
import StoreDetailsForm from 'src/components/StoreDetailsForm';
import PizzaLoader from './PizzaLoader';

const FormItem = Form.Item;
const Option = Select.Option;

interface Props {
  app: any;
}
interface State { }

class StoreDetails extends React.Component<Props, State> {

  handleSubmit = (e) => {
    
  }

  handleChange(value) {
    console.log(`selected ${value}`);
  }

  componentDidMount() {
    Actions.getShops();
  }

  render() {
    //if page is still loading data, show loading spinner
    if(this.props.app.shops.isFetching){
      return(
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
          <Col md={2}>
            {/* <Spin size="large" /> */}
            <PizzaLoader />
          </Col>
        </Row>
      );
    }
    //if fetch failed
    if(this.props.app.shops.isError){
      return(
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
          <Col md={12}>
            <Alert
              message="Connection Error"
              description="Please check your internet connection and reload this page."
              type="error"
              showIcon
            />
          </Col>
        </Row>
      );
    }

    //we have shops
    return (
      <>
        <h1 style={{ textAlign: 'center', fontSize: '2em', marginBottom: 40 }}>Store Details</h1>
        <StoreDetailsForm shop={this.props.app.shops.shops[0]} />
      </>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    app: state.app,
  };
}
export default connect(mapStateToProps)(StoreDetails);