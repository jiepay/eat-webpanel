import * as React from 'react';
import {Button, Layout, Menu, Spin, notification, Row, Col, Card, Tabs, Table, Modal, Input, Alert, Select, Tooltip} from 'antd';
import { CalendarMode } from 'antd/lib/calendar';
import { Moment } from 'moment';
import * as moment from 'moment';
import { connect } from 'react-redux';
import {Link, Switch, Route} from 'react-router-dom';
import axios from 'axios';
import ls from 'local-storage';

import SiderPageLayout from './SiderPageLayout';
import Utils from '../Utils';
import Actions from './Actions';
import AvatarDisplay from './AvatarDisplay';
import SiderMenu from './SiderMenu';
import OrdersTable from 'src/components/OrdersTable';
import Customers from './Customers';
import Orders from 'src/components/Orders';
import FoodMenu from 'src/components/FoodMenu';
import StoreDetails from 'src/components/StoreDetails';

const { Header, Sider, Content, Footer } = Layout;

interface Props{
  app: any;
  auth: any;
  page: string;
  dispatch?: any;
}
interface State{}
class AdminDashboard extends React.Component<Props, State> {
  openNotificationWithIcon = (type: string, title: string, description: string) => {
    notification[type]({
      message: title,
      description: description,
    });
  };

  onTypeFilterChanged = (value) => {
    this.setState({typeFilter: value});
  }

  componentDidMount() {
    // Actions.getUsers();
    // Actions.getShops();
  }
  
  render() {
    const l = this.props.app.locale;
    
    return (
      <SiderPageLayout siderMenu={<SiderMenu page={this.props.page} />}>
        <Row type="flex" justify="space-around" style={{width: '100%'}}>
          <Col md={22}>
            <Card style={{backgroundColor: '#fff', minHeight: '70vh'}}>
              <Switch>
                <Route path="/orders" render={(props: any) => (
                  <Orders />
                )} />
                <Route path="/customers" render={(props: any) => (
                  <Customers />
                )} />
                <Route path="/foodmenu" render={(props: any) => (
                  <FoodMenu />
                )} />
                <Route path="/store" render={(props: any) => (
                  <StoreDetails />
                )} />
              </Switch>
            </Card>
          </Col>
        </Row>
      </SiderPageLayout>
    );
  }
}

export default connect()(AdminDashboard);