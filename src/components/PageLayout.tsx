import * as React from 'react';
import { Layout, } from 'antd';
const { Content } = Layout;

import NavBar from './NavBar';
import Footer from './Footer';
import '../res/app.less';

interface Props{
  navHeader?: React.ReactNode;
  navTitle?: string;
}
interface State{}

export default class PageLayout extends React.Component<Props, State> {
  render() {
    let header = this.props.navHeader ?
      this.props.navHeader :
      (<NavBar type="blank" />);
    return (
      <Layout className="Reader" style={{ minHeight: '100vh' }}>
        <Layout className="content-layout" style={{ minHeight: '100vh', display: 'flex' }}>
          {header}
           {/* trackStyle={{marginTop: '0px'}}> */}
          <Content style={{flex: '1 1 auto', flexDirection: 'column', padding: '20px'}}>
            {/* , marginTop: 0 }}> */}
            {this.props.children}
          </Content>
          <Footer />
        </Layout>
      </Layout>
    );
  }
}