import * as React from 'react';
import { Row, Col, Button, } from 'antd';
import PageLayout from './PageLayout';
import { Link } from 'react-router-dom';

interface Props{}
interface State{}

export default class PageNotFound extends React.Component<Props, State> {
  render() {
    return (
      <PageLayout>
        <Row type="flex" justify="space-around" align="top" style={{flex: '1 1 auto'}}>
          <Col md={16} style={{textAlign: 'center'}}>
            <h2>Uh-oh, Page not found!</h2>
            <p>
              Sorry, we could not find the page you're looking for. It may have been moved or you visited an invalid link.
            </p>
            <Row type="flex" justify="space-around" align="middle" style={{marginTop: '50px'}}>
                <Link to="/"><Button type="primary">Go Back To Home</Button></Link>
            </Row>
          </Col>
        </Row>
      </PageLayout>
    );
  }
}