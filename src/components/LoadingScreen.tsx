import * as React from 'react';
import {Layout, Menu, Icon, Row, Col, Card, Spin, Alert,} from 'antd';
import { connect } from 'react-redux';
import PageLayout from './PageLayout';


interface Props{
  isLoading: boolean;
  isError: boolean;
  message: string;
  description: string;
  locale: string;
  app: any;
  auth: any;
}
interface State{}

class LoadingScreen extends React.Component<Props, State> {
  render() {
    return(
      <PageLayout>
        <Row type="flex" justify="space-around" style={{width: '100%'}}>
          <Col md={22}>
            <Card style={{backgroundColor: '#fff', minHeight: '70vh'}}>
              <Row type="flex" justify="center" align="middle" style={{height: '50vh'}}>
                {
                  this.props.isLoading ? 
                    //if page is still loading data, show loading spinner
                    <Col md={2}>
                      <Spin size="large" />
                    </Col>
                  :
                    <Col md={12}>
                      <Alert
                        message={this.props.message}
                        description={this.props.description}
                        type="error"
                        showIcon
                      />
                    </Col>
                }
              </Row>
            </Card>
          </Col>
        </Row>
      </PageLayout>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    app: state.app,
  };
}

export default connect(mapStateToProps)(LoadingScreen);