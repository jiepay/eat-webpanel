import * as React from 'react';
import { Layout } from 'antd';

interface Props{}
interface State{}

export default class Footer extends React.Component<Props, State> {
  render() {
    return (
      <Layout.Footer style={{ textAlign: 'center' }}>
        Reovo ©2018 All Rights Reserved
      </Layout.Footer>
    );
  }
}