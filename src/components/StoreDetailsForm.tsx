import * as React from 'react';
import { Layout, Spin, Alert } from 'antd';
import OrdersTable from 'src/components/OrdersTable';
import { Form, Input, InputNumber, Select, Row, Col, DatePicker, TimePicker, Button, notification } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { connect } from 'react-redux';
import Actions from './Actions';
import * as moment from 'moment';

const FormItem = Form.Item;
const Option = Select.Option;

interface Props extends FormComponentProps {
  shop?: any;
}
interface State { }

class StoreDetailsForm extends React.Component<Props, State> {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        Actions.updateStoreDetails(values.store, values.address1, values.address2, values.longitude, values.latitude, values.opening, values.closing, values.openingdays, values.phone)
        
        notification.success({
            message: 'Eat Food Hungry',
            description: 'Store details have been updated',
            style: {
              width: 600,
              marginLeft: 335 - 600,
            },
          });
        
      }
    });
  }

  handleChange(value) {
    console.log(`selected ${value}`);
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    const days: any[] = [
      { value: "monday", label: "Monday" },
      { value: "tuesday", label: "Tuesday" },
      { value: "wednesday", label: "Wednesday" },
      { value: "thursday", label: "Thursday" },
      { value: "friday", label: "Friday" },
      { value: "saturday", label: "Saturday" },
      { value: "sunday", label: "Sunday" },
      { value: "holiday", label: "Public Holiday" },
    ];
    const children = days.map((day, index) => <Option key={index} value={day.value}>{day.label}</Option>)

    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '230',
    })(
      <Select style={{ width: 80 }}>
        <Option value="230">+230</Option>
      </Select>
    );
    const shop = this.props.shop;
    if(!shop){
      console.log("No shop defined");
    }
    const days_open: string[] = [];
    shop && shop.open_mon && days_open.push("monday");
    shop && shop.open_tue && days_open.push("tuesday");
    shop && shop.open_wed && days_open.push("wednesday");
    shop && shop.open_thu && days_open.push("thursday");
    shop && shop.open_fri && days_open.push("friday");
    shop && shop.open_sat && days_open.push("saturday");
    shop && shop.open_sun && days_open.push("sunday");
    shop && shop.open_public_hols && days_open.push("holiday");

    return (
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            {...formItemLayout}
            label="Store Name"
          >
            {getFieldDecorator('store', {
              initialValue: shop && shop.name,
              rules: [{
                required: true, message: 'Please input the store name!',
              }],
            })(
              <Input placeholder="Store name" />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Address 1"
          >
            {getFieldDecorator('address1', {
              initialValue: shop && shop.address1,
              rules: [{
                required: true, message: 'Please input the address!',
              }],
            })(
              <Input placeholder="Address 1" />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Address 2"
          >
            {getFieldDecorator('address2', {
              initialValue: shop && shop.address2,
              rules: [{

              }],
            })(
              <Input placeholder="address2" />
            )}
          </FormItem>

          <FormItem

            {...formItemLayout}
            label="Longitude"
          >
            {getFieldDecorator('longitude', {
              initialValue: shop && shop.longitude,
            })(
              <InputNumber
                min={-180}
                max={180}
              />
            )}


          </FormItem>

          <FormItem

            {...formItemLayout}
            label="Latitude"
          >
            {getFieldDecorator('latitude', {
              initialValue: shop && shop.latitude,
            })(
              <InputNumber
                min={-180}
                max={180}
              />
            )}

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Opening Hours"
          >
            {getFieldDecorator('opening', {
              initialValue: shop && moment("2018-12-12T" + shop.opening),
              rules: [{ type: 'object', required: true, message: 'Please select time!' }],
            })(
              <TimePicker format="HH:mm" />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Closing Hours"
          >
            {getFieldDecorator('closing', {
              initialValue: shop && moment("2018-12-12T" + shop.closing),
              rules: [{ type: 'object', required: true, message: 'Please select time!' }],
            })(
              <TimePicker format="HH:mm" />
            )}
          </FormItem>

          <FormItem

            {...formItemLayout}

            label="Opening Days"
          >
          {getFieldDecorator('openingdays', {
              initialValue: days_open,
              rules: [{ required: true, message: 'Please select open days!' }],
            })(
            <Select
              mode="multiple"
              style={{ width: '100%' }}
              placeholder="Please select"
              onChange={this.handleChange}
            >

              {children}

            </Select>
            )}

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Phone Number"
          >
            {getFieldDecorator('phone', {
              initialValue: shop && shop.phone,
              rules: [{ required: true, message: 'Please input your phone number!' }],
            })(
              <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
            )}
          </FormItem>

          <FormItem 
          {...formItemLayout}>
              <Button type="primary" htmlType="submit">Save</Button>
          </FormItem>
        </Form>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    app: state.app,
  };
}
export default connect(mapStateToProps)(Form.create()(StoreDetailsForm));