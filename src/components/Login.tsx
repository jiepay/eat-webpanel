import * as React from 'react';
import { Row, Col, Card, } from 'antd';
import {Link} from 'react-router-dom';
import PageLayout from './PageLayout';
// import { Link } from 'react-router-dom';
import LoginForm from './LoginForm';

interface Props{
  locale: string;
  auth: any;
}
interface State{}

export default class Login extends React.Component<Props, State> {
  render() {
    return (
      <PageLayout>
        <Row type="flex" justify="space-around" align="top" style={{flex: '1 1 auto'}}>
          <Col md={8}>
            <Card hoverable={false} title={<Row><h3 style={{textAlign: 'center'}}>Admin Login</h3></Row>}>
              <LoginForm locale={this.props.locale} auth={this.props.auth} />
            </Card>
          </Col>
        </Row>
      </PageLayout>
    );
  }
}
