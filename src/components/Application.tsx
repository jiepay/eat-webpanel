// import * as React from 'react';
// import {Button, Layout, Menu, Spin, notification, Row, Col, Card, message} from 'antd';
// import { CalendarMode } from 'antd/lib/calendar';
// import { Moment } from 'moment';
// import * as moment from 'moment';
// import { connect } from 'react-redux';
// import {Link} from 'react-router-dom';

// import ApplicationForm from './ApplicationForm';
// import PageLayout from './PageLayout';
// import Actions from './Actions';

// const { Header, Sider, Content, Footer } = Layout;

// interface Props{
//   app: any; //entire app state
//   auth: any; //entire app state
//   dispatch?: any;
// }
// interface State{
//   photoNull: boolean;
// }
// class Application extends React.Component<Props, State> {
//   state = {
//     photoNull: false,
//   }
//   formRef;
//   openNotificationWithIcon = (type: string, title: string, description: string) => {
//     notification[type]({
//       message: title,
//       description: description,
//     });
//   };
//   applyOk = (e) => {
//     console.log(this.formRef);
//     //submit form
//     const form = this.formRef.props.form;
//     form.validateFields((err, values) => {
//       console.log("Validating form fields...");
//       if (err) {
//         console.log("error:");
//         console.log(err);
//         message.error("Please fill required fields", 10);
//         if(this.formRef.state.photoFile == null){
//           console.log("Photo file is also null");
//           this.setState({photoNull: true});
//         }
//         return;
//       }
//       if(this.formRef.state.photoFile == null){
//         console.log("Photo file is null");
//         message.error("Please select a photo", 10);
//         this.setState({photoNull: true});
//         return;
//       }
      
//       //submit form
//       console.log('Received values of form: ', values);
//       Actions.createApplication(values, this.formRef.state.photoFile, this.formRef.state.crop, (success) => {
//         if(success){
//           this.openNotificationWithIcon("success", "Success", "Accreditation was created successfully");
//           form.resetFields();
//           this.setState({photoNull: false});
//           this.formRef.setState({
//             photoFile: null,
//             isImageDirty: false,
//             imageZoom: 1,
//             isAthlete: false,
//             isUploading: false,
//             isSubmitting: false,
//             cropX: 0.5,
//             cropY: 0.5,
//             crop: {
//               x: 0,
//               y: 0,
//               width: 0,
//               height: 0,
//             },
//           });
//         }
//         else{
//           this.openNotificationWithIcon("error", "Error", "An error occurred");
//         }
//       });
//     });
//   }

//   saveFormRef = (formRef) => {
//     this.formRef = formRef;
//   }

//   componentDidMount() {
    
//   }
  
//   render() {
//     return (
//       <PageLayout>
//         <Row type="flex" justify="space-around" style={{width: '100%'}}>
//           <Col md={16}>
//             <Card style={{backgroundColor: '#fff'}}>
//               <Row type="flex">
//                 <Col md={4}>
//                   <Link to="/">
//                     <Button icon="left">Go Back</Button>
//                   </Link>
//                 </Col>
//               </Row>
//               <h1 style={{textAlign: 'center', fontSize: '2em', marginBottom: 40}}>{this.props.app.locale === "en" ? "Accreditation Form" : "Demande d'Accreditation"}</h1>
//               <ApplicationForm
//                 wrappedComponentRef={this.saveFormRef}
//                 app={this.props.app}
//                 auth={this.props.auth}
//                 photoNull={this.state.photoNull} />
//               <Row type="flex" justify="center">
//                 <Col md={4}>
//                   <Button
//                     size="large"
//                     className="default-btn"
//                     style={{
//                       marginTop: 20,
//                       borderRadius: 20,
//                     }}
//                     loading={this.props.app.applications.create.isFetching}
//                     disabled={this.props.app.applications.create.isFetching}
//                     onClick={this.applyOk}>
//                     {this.props.app.locale === "en"? "Submit Application" : "Valider L'application"}
//                   </Button>
//                 </Col>
//               </Row>
//             </Card>
//           </Col>
//         </Row>
//       </PageLayout>
//     );
//   }
// }

// export default connect()(Application);