// import * as React from 'react';
// import { Icon, Button,
//   Form, Input, Checkbox, Row, Alert, Col, Select, message, notification } from 'antd';
// import {FormComponentProps} from 'antd/lib/form/Form';
// import { Link } from 'react-router-dom';
// import { connect } from 'react-redux';
// import { appSetAccountType, appSetIsInitializing } from '../actions';
// import ls from 'local-storage';
// import Actions from './Actions';

// const FormItem = Form.Item;

// interface Props extends FormComponentProps{
//   locale?: string;
//   dispatch?: any;
//   auth?: any;
//   app?: any;
//   admin?: any;
// }
// interface State{
//   confirmDirty: boolean;
//   hasTried: boolean;
// }

// class CreateAdminForm extends React.Component<Props, State> {
//   constructor(props: FormComponentProps){
//     super(props);
//     this.state = {
//       confirmDirty: false,
//       hasTried: false,
//     };
//   }
//   openNotificationWithIcon = (type: string, title: string, description: string) => {
//     notification[type]({
//       message: title,
//       description: description,
//     });
//   };
//   handleSubmit = (e) => {
//     e.preventDefault();
//     const l = this.props.app.locale;
//     this.setState({hasTried: true});
//     this.props.form.validateFields((err, values) => {
//       if(err){
//         message.error(translations(l).plsFillRequired, 10);
//       }
//       else{
//         if(process.env.NODE_ENV === "development"){
//           console.log('Received values of form: ', values);
//         }
//         const email = (values.email as string).trim().toLowerCase();
//         if(this.props.admin){
//           //edit admin
//           Actions.editAdmin(this.props.admin.id, values, (success)=>{
//             if(success){
//               this.openNotificationWithIcon("success", translations(l).success, translations(l).updateAdminSuccess);
//               this.props.form.resetFields();
//             }
//             else{
//               this.openNotificationWithIcon("error", translations(l).error, translations(l).errorOccurred);
//             }
//           });
//         }
//         else{
//           Actions.createAdmin(values, (success)=>{
//             if(success){
//               this.openNotificationWithIcon("success", translations(l).success, translations(l).createAdminSuccess);
//               this.props.form.resetFields();
//             }
//             else{
//               this.openNotificationWithIcon("error", translations(l).error, translations(l).errorOccurred);
//             }
//           });
//         }
//       }
//     });
//   }

//   handleConfirmBlur = (e) => {
//     const value = e.target.value;
//     this.setState({ confirmDirty: this.state.confirmDirty || !!value });
//   }
//   compareToFirstPassword = (rule, value, callback) => {
//     const form = this.props.form;
//     if (value && value !== form.getFieldValue('password')) {
//       callback(translations(this.props.app.locale).pwdMatch);
//     } else {
//       callback();
//     }
//   }
//   validateToNextPassword = (rule, value, callback) => {
//     const form = this.props.form;
//     if (value && this.state.confirmDirty) {
//       form.validateFields(['password2'], { force: true, }, ()=>{});
//     }
//     callback();
//   }

//   render() {
//     const l = this.props.app.locale;
//     const { getFieldDecorator } = this.props.form;
//     const itemLayout = {
//       labelCol: {
//         xs: { span: 24 },
//         sm: { span: 24 },
//       },
//       wrapperCol: {
//         xs: { span: 24 },
//         sm: { span: 24 },
//       },
//     };
//     let errorMsg = "";
//     if(this.state.hasTried && this.props.app.users.create.isError){
//       if(!this.props.app.users.create.error.response && this.props.app.users.create.error.request){
//         errorMsg = translations(l).connectionError;
//       }
//       else if(this.props.app.users.create.error.response.status === 400 || this.props.app.users.create.error.response.status === 401){
//         errorMsg = translations(l).incorrectEmailOrPwd;
//       }
//       else if(this.props.app.users.create.error.response.status === 422){
//         errorMsg = translations(l).emailAlreadyRegistered;
//       }
//       else if(this.props.app.users.create.error.response.status === 500){
//         errorMsg = translations(l).serverError;
//       }
//       else{
//         errorMsg = "Unknown error";
//       }
//     }
//     const error = (
//       <Row>
//         <Alert message={errorMsg} type="error" showIcon />
//         <div style={{height: 30}}></div>
//       </Row>
//     );
//     console.log(this.props.admin);
//     return (
//       <Form onSubmit={this.handleSubmit} hideRequiredMark={true} className="reg-form">
//         {this.state.hasTried && this.props.app.users.create.isError && error}
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).firstName} colon={false}>
//               {getFieldDecorator('first_name', {
//                 initialValue: this.props.admin && this.props.admin.first_name,
//                 rules: [{
//                   required: true,
//                   message: translations(l).plsFillFirstName,
//                   whitespace: true
//                 }],
//               })(
//                 <Input size="large" />
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).lastName} colon={false}>
//               {getFieldDecorator('last_name', {
//                 initialValue: this.props.admin && this.props.admin.last_name,
//                 rules: [{
//                   required: true,
//                   message: translations(l).plsFillLastName,
//                   whitespace: true
//                 }],
//               })(
//                 <Input size="large" />
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem label={translations(l).email} colon={false}>
//               {getFieldDecorator('email', {
//                 initialValue: this.props.admin && this.props.admin.email,
//                 rules: [
//                   { type: 'email', message: translations(l).plsFillValidEmail, },
//                   { required: true, message: translations(l).plsFillEmail },
//                 ],
//               })(
//                 <Input
//                   prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
//                   size="large"
//                   placeholder={translations(l).email} />
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).country} colon={false}>
//               {getFieldDecorator('country', {
//                 initialValue: this.props.admin && this.props.admin.country.toUpperCase(),
//                 rules: [{
//                   required: true,
//                   message: translations(l).plsFillCountry,
//                   whitespace: true
//                 }],
//               })(
//                 <Select size="large" placeholder={translations(l).pleaseSelect}>
//                   {countries.map((country: {name: string; code: string;}, index: number) => (
//                     <Select.Option key={country.code} value={country.code}>{country.name}</Select.Option>
//                   ))}
//                 </Select>
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem label={translations(l).password} colon={false}>
//               {getFieldDecorator('password', {
//                 initialValue: this.props.admin && this.props.admin.dept,
//                 rules: [
//                   { required: true, message: translations(l).plsFillPwd },
//                   { min: 8, message: translations(l).plsFillPwdChars },
//                   { validator: this.validateToNextPassword, },
//                 ],
//               })(
//                 <Input
//                   prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
//                   type="password"
//                   size="large"
//                   placeholder={translations(l).password} />
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem label={translations(l).confirmPassword} colon={false}>
//               {getFieldDecorator('password2', {
//                 initialValue: this.props.admin && this.props.admin.dept,
//                 rules: [
//                   { required: true, message: translations(l).plsFillPwd },
//                   { validator: this.compareToFirstPassword, },
//                 ],
//               })(
//                 <Input
//                   prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
//                   type="password"
//                   size="large"
//                   placeholder={translations(l).confirmPassword}
//                   onBlur={this.handleConfirmBlur} />
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <FormItem>
//           <Row type="flex" justify="center">
//             <Button
//               size="large"
//               className="default-btn"
//               style={{
//                 marginTop: 20,
//                 borderRadius: 20,
//               }}
//               loading={this.props.app.users.create.isFetching}
//               disabled={this.props.app.users.create.isFetching}
//               htmlType="submit">
//               {this.props.admin ? translations(l).saveChanges : translations(l).createAdmin}
//             </Button>
//           </Row>
//         </FormItem>
//       </Form>
//     );
//   }
// }

// export default connect()(Form.create()(CreateAdminForm));
