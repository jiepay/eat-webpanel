// import * as React from 'react';
// import { Form, Icon, Input, Button, Checkbox, DatePicker, TimePicker, Select, Col, Row, InputNumber, message, Upload, Slider } from 'antd';
// import { connect } from 'react-redux';
// import AvatarEditor from 'react-avatar-editor';

// import { Moment } from 'moment';
// import * as moment from 'moment';
// import Utils from '../Utils';

// const FormItem = Form.Item;

// interface Props{
//   app: any;
//   auth: any;
//   application?: any;
//   photoNull: boolean;
//   dispatch?: any;
//   form: any;
//   wrappedComponentRef?: any;
// }
// interface State{
//   photoFile: any;
//   isImageDirty: boolean;
//   imageZoom: number;
//   isAthlete: boolean;
//   isUploading: boolean;
//   isSubmitting: boolean;
//   cropX: number;
//   cropY: number;
//   crop: any;
// }
// class ApplicationForm extends React.Component<Props, State> {
//   state = {
//     photoFile: null,
//     isImageDirty: false,
//     imageZoom: 1,
//     isAthlete: false,
//     isUploading: false,
//     isSubmitting: false,
//     cropX: 0.5,
//     cropY: 0.5,
//     crop: {
//       x: 0,
//       y: 0,
//       width: 0,
//       height: 0,
//     },
//   };

//   handleSubmit = (e) => {
//     e.preventDefault();
//     // this.props.form.validateFields((err, values) => {
//     //   if (!err) {
//     //     console.log('Received values of form: ', values);
//     //   }
//     // });
//   }
//   getBase64 = (img, callback) => {
//     const reader = new FileReader();
//     reader.addEventListener('load', () => callback(reader.result));
//     reader.readAsDataURL(img);
//   }
  
//   beforeUpload = (file) => {
//     this.setState({photoFile: file, isImageDirty: true});
//     return false;
//     // const isJPG = file.type === 'image/jpeg';
//     // const isPNG = file.type === 'image/png';
//     // console.log(file.type);
//     // if (!isJPG && !isPNG) {
//     //   message.error('You can only upload JPG or PNG file!');
//     // }
//     // const isLt2M = file.size / 1024 / 1024 < 2;
//     // if (!isLt2M) {
//     //   message.error('Image must smaller than 2MB!');
//     // }
//     // return isJPG && isLt2M;
//   }

//   onAvatarUploadChange = (info) => {
//     console.log(info); //uploading, done, error, removed
//     if (info.file.status === 'uploading') {
//       this.setState({isUploading: true});
//       return;
//     }
//     if (info.file.status === 'done') {
//       console.log('done');
//       // Get this url from response in real world.
//       this.getBase64(info.file.originFileObj, imageUrl => {
//         this.setState({
//           isUploading: false,
//         });
//       });
//     }
//     if(info.file.status === 'error'){
//       this.setState({isUploading: false});
//       console.log("error uploading");
//     }
//   }

//   onStatusChanged = (value) => {
//     console.log("Status selected");
//     if(value){
//       this.setState({isAthlete: value.toLowerCase() === "athlete"});
//     }
//   }

//   onCropChanged = (crop) => {
//     this.setState({ crop });
//     console.log(crop);
//   }

//   dobDisabledDate = (current) => {
//     // Can not select days after 10 years ago today
//     return current && current > moment().subtract(10, "years");
//   }
//   expDisabledDate = (current) => {
//     // Can not select days before today and today
//     return current && current < moment().endOf('day');
//   }
//   onDrag = (value) => {
//     // console.log("Value!!!");
//     // console.log(value);
//     if(value && value.x && value.y){
//       this.setState({
//         cropX: value.x,
//         cropY: value.y,
//         crop: (this as any).editor.getCroppingRect(),
//       });
//     }
//     // this.setState({
//     // });
//   }
//   onZoom = (value) => {
//     this.setState({
//       imageZoom: value,
//       crop: (this as any).editor.getCroppingRect(),
//     });
//   }
//   zoomOut = () => {
//     let zoom = Math.max(this.state.imageZoom - 0.5, 1);
//     this.setState({
//       imageZoom: zoom,
//       crop: (this as any).editor.getCroppingRect(),
//     });
//   }
//   zoomIn = () => {
//     let zoom = Math.min(this.state.imageZoom + 0.5, 5);
//     this.setState({
//       imageZoom: zoom,
//       crop: (this as any).editor.getCroppingRect(),
//     });
//   }
//   setEditorRef = (editor) => (this as any).editor = editor
  
//   componentDidMount(){
//     if(this.props.application){
//       this.onStatusChanged(engStatuses.filter(s => s.toLowerCase() === this.props.application.status)[0]);
//       let app = this.props.application;
//       // let zoom = 1;
//       if(app.photo){
//         const img = new Image();
//         img.src = app.photo;
//         img.onload = (e)=>{
//           console.log(img.width);
//           console.log(app.photo_crop_width);
//           console.log((img.width / (35 * 4)) / app.photo_crop_width);
//           const zoom = (img.width / (35 * 4)) / (app.photo_crop_width * 10);
//           this.setState({imageZoom: Math.max(1, Math.min(5, zoom))})
//         };
//         // zoom = img.width * app.photo_crop_width;
//         this.setState({
//           photoFile: app.photo,
//           // imageZoom: zoom,
//           cropX: app.photo_crop_x + (app.photo_crop_width / 2),
//           cropY: app.photo_crop_y + (app.photo_crop_height / 2),
//           crop: {
//             x: app.photo_crop_x,
//             y: app.photo_crop_y,
//             width: app.photo_crop_width,
//             height: app.photo_crop_height,
//           }
//         });
//       }
//     }
//   }
//   render() {
//     const l = this.props.app.locale;
//     const photoSize = 4;
//     const { getFieldDecorator } = this.props.form;
//     const itemLayout = {
//       labelCol: {
//         xs: { span: 24 },
//         sm: { span: 24 },
//       },
//       wrapperCol: {
//         xs: { span: 24 },
//         sm: { span: 24 },
//       },
//     };
//     const uploadButton = (
//       <Row
//         type="flex"
//         justify="center"
//         align="middle"
//         style={{
//           border: this.props.photoNull ? '1px solid red' : 'none',
//           width: '100%',
//           height: '100%',
//           transform: 'scale(1.15, 1.11)',
//           borderRadius: 3,
//         }}>
//         <Col span={24}>
//           <Icon type={this.state.isUploading ? 'loading' : 'plus'} style={{fontSize: '5em'}} />
//         </Col>
//         <Col span={24}>
//           <div className="ant-upload-text">{translations(l).uploadPhoto}</div>
//         </Col>
//       </Row>
//     );
//     let app = this.props.application;
//     let photo = this.state.photoFile;

//     let disc;
//     if(app){
//       disc = app.discipline.split(",");
//       disc = disc.map((d) => Utils.capitalize(d));
//     }

//     return (
//       <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
//         <Row type="flex" justify="end">
//           <Col md={6}>
//             {
//               photo == null ?
//               <>
//                 <Upload
//                   accept="image/jpeg, image/png"
//                   name="avatar"
//                   listType="picture-card"
//                   showUploadList={false}
//                   action="/avatars/"
//                   beforeUpload={this.beforeUpload}
//                   onChange={this.onAvatarUploadChange}
//                   style={{width: 35 * photoSize, height: 45 * photoSize}}
//                 >
//                   {uploadButton}
//                 </Upload>
//                 <div style={{height: 20}}></div>
//               </>
//               :
//               <>
//                 <AvatarEditor
//                   ref={this.setEditorRef}
//                   image={photo}
//                   width={35 * photoSize}
//                   height={45 * photoSize}
//                   position={{x: this.state.cropX, y: this.state.cropY}}
//                   border={0}
//                   color={[255, 255, 255, 0.6]} // RGBA
//                   scale={this.state.imageZoom}
//                   rotate={0}
//                   onImageReady={this.onDrag}
//                   onPositionChange={this.onDrag}
//                 />
//                 <div style={{width: 35 * photoSize}} className="slider-icon-wrapper">
//                   <Icon className="hover-active cursor-pointer" onClick={this.zoomOut} type="minus-circle-o" />
//                   <Slider min={1} max={5} onChange={this.onZoom} step={0.0001} value={this.state.imageZoom} />
//                   <Icon className="hover-active cursor-pointer" onClick={this.zoomIn} type="plus-circle-o" />
//                 </div>
//                 <Upload
//                   accept="image/jpeg, image/png"
//                   name="avatar"
//                   showUploadList={false}
//                   action="/avatars/"
//                   beforeUpload={this.beforeUpload}
//                   onChange={this.onAvatarUploadChange}
//                 >
//                   <Button className="default-btn">
//                     <Icon type="upload" /> {translations(l).changePic}
//                   </Button>
//                 </Upload>
//                 <div style={{height: 30}}></div>
//               </>
//             }
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).firstName} colon={false}>
//               {getFieldDecorator('name', {
//                 initialValue: app && app.name,
//                 rules: [{
//                   required: true,
//                   message: 'Please enter your name',
//                   whitespace: true
//                 }],
//               })(
//                 <Input size="large" />
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).lastName} colon={false}>
//               {getFieldDecorator('last_name', {
//                 initialValue: app && app.last_name,
//                 rules: [{
//                   required: true,
//                   message: 'Please enter your last name',
//                   whitespace: true
//                 }],
//               })(
//                 <Input size="large" />
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).country} colon={false}>
//               {getFieldDecorator('country', {
//                 initialValue: app && app.country.toUpperCase(),
//                 rules: [{
//                   required: true,
//                   message: 'Please enter your country',
//                   whitespace: true
//                 }],
//               })(
//                 <Select size="large" placeholder={translations(l).pleaseSelect}>
//                   {countries.map((country: {name: string; code: string;}, index: number) => (
//                     <Select.Option key={country.code} value={country.code}>{country.name}</Select.Option>
//                   ))}
//                 </Select>
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).nationality} colon={false}>
//               {getFieldDecorator('nationality', {
//                 initialValue: app && app.nationality.toUpperCase(),
//                 rules: [{
//                   required: true,
//                   message: 'Please select your nationality',
//                   whitespace: true
//                 }],
//               })(
//                 <Select size="large" placeholder={translations(l).pleaseSelect} onChange={()=>{console.log("Nationality selected")}}>
//                   {countries.map((country: {name: string; code: string;}, index: number) => (
//                     <Select.Option key={country.code} value={country.code}>{country.name}</Select.Option>
//                   ))}
//                 </Select>
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).dob} colon={false}>
//               {getFieldDecorator('dob', {
//                 initialValue: app && moment(app.dob),
//                 rules: [{
//                   required: true,
//                   message: 'Please enter a date of birth',
//                 }],
//               })(
//                 <DatePicker
//                   size="large"
//                   disabledDate={this.dobDisabledDate}
//                   defaultPickerValue={moment().subtract(10, "years")}
//                   showToday={false} />
//               )}
//             </FormItem>
//           </Col>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={translations(l).gender} colon={false}>
//               {getFieldDecorator('gender', {
//                 initialValue: app && app.gender,
//                 rules: [{
//                   required: true,
//                   message: 'Please select your gender',
//                   whitespace: true
//                 }],
//               })(
//                 <Select size="large" placeholder={translations(l).pleaseSelect} onChange={()=>{console.log("Gender selected")}}>
//                   <Select.Option key={1} value="M">Male</Select.Option>
//                   <Select.Option key={2} value="F">Female</Select.Option>
//                 </Select>
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         <Row gutter={50}>
//           <Col md={12}>
//             <FormItem {...itemLayout} label={"Passport Expiry Date"} colon={false}>
//               {getFieldDecorator('passport_exp', {
//                 initialValue: app && moment(app.passport_exp),
//                 rules: [{
//                   required: false,
//                   message: 'Please enter the expiry date of your passport',
//                 }],
//               })(
//                 <DatePicker
//                   size="large"
//                   disabledDate={this.expDisabledDate}
//                   showToday={false} />
//               )}
//             </FormItem>
//           </Col>
//         </Row>
//         }
//       </Form>
//     );
//   }
// }

// export default connect()(Form.create()(ApplicationForm));