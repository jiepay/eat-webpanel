// import * as React from 'react';
// import {Button, Layout, Menu, Spin, notification, Row, Col, Card, message} from 'antd';
// import { CalendarMode } from 'antd/lib/calendar';
// import { Moment } from 'moment';
// import * as moment from 'moment';
// import { connect } from 'react-redux';
// import {Redirect, Link} from 'react-router-dom';

// import ApplicationForm from './ApplicationForm';
// import PageLayout from './PageLayout';
// import Actions from './Actions';
// import CardDisplay from './CardDisplay';

// const { Header, Sider, Content, Footer } = Layout;

// interface Props{
//   app: any; //entire app state
//   auth: any;
//   application: any;
//   dispatch?: any;
//   match?: any;
// }
// interface State{
//   applyIsSubmitting: boolean;
// }
// class EditApplication extends React.Component<Props, State> {
//   state = {
//     applyIsSubmitting: false,
//   };
//   formRef;

//   openNotificationWithIcon = (type: string, title: string, description: string) => {
//     notification[type]({
//       message: title,
//       description: description,
//     });
//   };
//   applyOk = (e) => {
//     //submit form
//     const form = this.formRef.props.form;
//     form.validateFields((err, values) => {
//       console.log("Validating form fields...");
//       if (err) {
//         console.log("error:");
//         console.log(err);
//         message.error("Please fill required fields", 10);
//         if(this.formRef.state.photoFile == null){
//           console.log("Photo file is also null");
//           // this.setState({photoNull: true});
//         }
//         return;
//       }
//       if(this.formRef.state.photoFile == null){
//         console.log("Photo file is null");
//         message.error("Please select a photo", 10);
//         // this.setState({photoNull: true});
//         return;
//       }
      
//       //submit form
//       console.log('Received values of form: ', values);
//       let application;
//       if(this.props.application){
//         application = this.props.application;
//       }
//       else if(this.props.match.params.id){
//         application = this.props.app.applications.applications.filter(a => a.id === parseInt(this.props.match.params.id))[0];
//       }
//       let photoFile = typeof this.formRef.state.photoFile !== "string" && this.formRef.state.photoFile;
//       Actions.modifyApplication(application.id, values, photoFile, this.formRef.state.crop, (success) => {
//         if(success){
//           this.openNotificationWithIcon("success", "Success", "Accreditation was updated successfully");
//           // form.resetFields();
//         }
//         else{
//           this.openNotificationWithIcon("error", "Error", "An error occurred");
//         }
//       });
//     });
//   }

//   saveFormRef = (formRef) => {
//     this.formRef = formRef;
//   }

//   componentDidMount() {
    
//   }
  
//   render() {
//     const l = this.props.app.locale;
//     let application;
//     if(this.props.application){
//       application = this.props.application;
//     }
//     else if(this.props.match.params.id){
//       application = this.props.app.applications.applications.filter(a => a.id === parseInt(this.props.match.params.id))[0];
//     }
//     if(application == null){
//       return <Redirect to={{pathname: '/'}}/>
//     }

//     if(application.approved){
//       return <CardDisplay app={this.props.app} auth={this.props.auth} />
//     }

//     return (
//       <PageLayout>
//         <Row type="flex" justify="space-around" style={{width: '100%'}}>
//           <Col md={16}>
//             <Card style={{backgroundColor: '#fff'}}>
//               <Row type="flex">
//                 <Col md={4}>
//                   <Link to="/">
//                     <Button icon="left">Go Back</Button>
//                   </Link>
//                 </Col>
//               </Row>
//               <h1 style={{textAlign: 'center', fontSize: '2em', marginBottom: 40}}>{translations(l).editAccred}</h1>
//               <ApplicationForm
//                 wrappedComponentRef={this.saveFormRef}
//                 app={this.props.app}
//                 auth={this.props.auth}
//                 application={application} />
//               <Row type="flex" justify="center">
//                 <Col md={4}>
//                   <Button
//                     size="large"
//                     className="default-btn"
//                     style={{
//                       marginTop: 20,
//                       borderRadius: 20,
//                     }}
//                     loading={this.props.app.applications.create.isFetching}
//                     disabled={this.props.app.applications.create.isFetching}
//                     onClick={this.applyOk}>
//                     {translations(l).saveChanges}
//                   </Button>
//                 </Col>
//               </Row>
//             </Card>
//           </Col>
//         </Row>
//       </PageLayout>
//     );
//   }
// }

// export default connect()(EditApplication);