import * as React from 'react';
import {Button, Layout, Menu, Spin, notification, Row, Col, Card, Tabs, Table, Modal, Input, Alert, Select, Tooltip} from 'antd';
import { CalendarMode } from 'antd/lib/calendar';
import { Moment } from 'moment';
import * as moment from 'moment';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import axios from 'axios';
import ls from 'local-storage';

import SiderPageLayout from './SiderPageLayout';
import Utils from '../Utils';
import Actions from './Actions';
import AvatarDisplay from './AvatarDisplay';
import SiderMenu from './SiderMenu';

const { Header, Sider, Content, Footer } = Layout;

interface Props{
  customers: any[];
}
interface State{
  activeOrder: any;
  modalOpen: boolean;
  modalEditable: boolean;
  search: string;
}
class CustomersTable extends React.Component<Props, State> {
  state = {
    activeOrder: null as any,
    modalOpen: false,
    modalEditable: false,
    search: "",
  };
  formRef;

  openNotificationWithIcon = (type: string, title: string, description: string) => {
    notification[type]({
      message: title,
      description: description,
    });
  };

  approve = (id: number) => {
    // Actions.approveApplication(id);
    this.setState({modalOpen: false, activeOrder: null});
  }
  
  render() {
    const filteredcustomers = this.props.customers.filter((app: any) => {
      const search = this.state.search.trim().toLowerCase();
      let pass = true;
      if(search.length > 0){
        //we have search term
        pass = false;
        const a = app;
        const mash: string[] = [
          a.id,
          a.first_name, 
          a.last_name,
          a.email,
          a.profile.phone_number,
        ];
        if(mash.join(" ").toLowerCase().indexOf(search) != -1){
          pass = true;
        }
      }
      return pass;
    });
    const customers = filteredcustomers.map((cust: any, index: number) => {
      return {
        key: cust.id,
        name: cust.first_name + " " + cust.last_name,
        email: cust.email,
        phone: cust.profile.phone_number,
      };
    });
    return (
      <>
        <Row>
          <Col md={8} style={{marginBottom: 20}}>
            <Input.Search
              placeholder={`Search...`}
              onSearch={value => console.log(value)}
              enterButton
              onChange={(e) => this.setState({search: e.target.value})}
            />
          </Col>
          {/* <Col md={8} offset={1}>
            <Select style={{ width: '100%' }} placeholder={translations(l).filterByType} onChange={this.onTypeFilterChanged}>
              <Select.Option key="-1" value="">All</Select.Option>
              <Select.Option key={0} value={status}>Blah</Select.Option>
            </Select>
          </Col> */}
        </Row>
        <Table
          dataSource={customers}
          onRow={(record: any) => {
            return {
              onClick: () => {
                console.log(record);
                const app = this.props.customers.filter(a => a.id === record.key)[0];
                this.setState({activeOrder: app, modalOpen: true, modalEditable: true});
              },
            };
          }}
          rowClassName={(record, index)=>"cursor-pointer"} >
          <Table.Column
            title={"ID"}
            dataIndex="key"
            key="id"
            />
          <Table.Column
            title={"Name"}
            dataIndex="name"
            key="name"
            />
          <Table.Column
            title={"Email"}
            dataIndex="email"
            key="email"
            />
          <Table.Column
            title={"Phone"}
            dataIndex="phone"
            key="phone"
            />
          {/* <Table.Column
            title={"Date Delivered"}
            dataIndex="date_delivered"
            key="date_delivered"
            />
          <Table.Column
            title={"Delivered"}
            // dataIndex="delivered"
            key="delivered"
            render={(text, record: any, index)=>
              <Button type="primary">Delivered</Button>
            }
            /> */}
          {/* <Table.Column
            title={"Cancelled"}
            // dataIndex="cancelled"
            key="cancelled"
            render={(text, record: any, index)=>
              <Button type="danger">Cancelled</Button>
            }
            /> */}
        </Table>
      </>
    );
  }
}

export default connect()(CustomersTable);