import * as React from 'react';
import { Layout, Row, Button, Icon } from 'antd';
const { Header, Content, Sider } = Layout;
// const SubMenu = Menu.SubMenu;
// const Search = Input.Search;

import NavBar from './NavBar';
import Footer from './Footer';
import '../res/app.less';

interface Props{
  navHeader?: React.ReactNode;
  navTitle?: string;
  sider?: React.ReactNode;
  siderHeader?: string;
  siderMenu?: React.ReactNode;
}
interface State{
  siderCollapsed: boolean;
}

export default class SiderPageLayout extends React.Component<Props, State> {
  constructor(props: Props){
    super(props);
    this.state = {
      siderCollapsed: false,
    };
  }
  toggleSiderCollapse = (collapsed) => {
    this.setState({ siderCollapsed: !this.state.siderCollapsed });
  }
  render() {
    let sider = this.props.sider ?
      this.props.sider :
      (
        <Sider
          className="sider"
          collapsible
          collapsedWidth="0"
          collapsed={this.state.siderCollapsed}
          trigger={null}
          // onCollapse={this.onCollapse}
          breakpoint="sm"
          style={{position: 'fixed', left: '0px', height: '100vh'}}
          >
          <Row style={{padding: '16px', backgroundColor: '#222'}}>
            <h1 className="sider-header">{this.props.siderHeader}</h1>
          </Row>
            {this.props.siderMenu}
        </Sider>
      );
    let header = this.props.navHeader ?
      this.props.navHeader :
      (<NavBar type="blank" />);
    return (
      <Layout className="Reader" style={{ minHeight: '100vh' }}>
        {sider}
        <Layout className="content-layout">
          {header}
          <Content style={{flex: '1 1 auto', padding: '30px', marginTop: 64}}>
            <Button type="primary" className="sider-trigger" onClick={this.toggleSiderCollapse}>
              <Icon type={this.state.siderCollapsed ? "right" : "left"} />
            </Button>
            {this.props.children} 
          </Content>
          <Footer />
        </Layout>
      </Layout>
    );
  }
}