import * as React from 'react';
import { Layout, Card, Row, Col, Button, Pagination, List, Tooltip, Modal, Popconfirm, Select, Input, Form, Alert } from 'antd';
import EditFood from './EditFood';
import PizzaLoader from 'src/components/PizzaLoader';
import {connect} from 'react-redux';
import Actions from './Actions';
import ReconnectingWebSocket from 'reconnecting-websocket';
import { appSetFoods } from 'src/actions';

const imgs = [
  require("../res/img/donuts_cr.jpg"),
  require("../res/img/donuts.jpg"),
  require("../res/img/doughnuts.jpg"),
  require("../res/img/mini_donuts.jpg"),
  require("../res/img/pie.jpg"),
  require("../res/img/pies.jpg"),
  require("../res/img/potato_pie.jpg"),
];

interface Props {
  foods: any;
  categories: any;
  dispatch?: any;
}
interface State {
  search: string;
  catFilter: string;
  currentPage: number;
  createModalOpen: boolean;
  editModalOpen: boolean;
  activeFood: any;
}

class FoodMenu extends React.Component<Props, State> {
  state = {
    search: "",
    catFilter: "",
    currentPage: 1,
    createModalOpen: false,
    editModalOpen: false,
    activeFood: null as any,
  }
  foodWS;
  catWS;

  showCreateModal = () => {
    this.setState({
      createModalOpen: true,
    });
  }

  showEditModal = (food) => {
    this.setState({
      editModalOpen: true,
      activeFood: food,
    });
  }
  handleOk = (e) => {
    console.log(e);
    this.setState({
      editModalOpen: false,
    });
  }
  handleCancelEdit = (e) => {
    console.log(e);
    this.setState({
      editModalOpen: false,
    });
  }
  
  onTypeFilterChanged = (value) => {
    this.setState({ catFilter: value });
  }
  
  onPageChanged = (page) => {
    this.setState({
      currentPage: page,
    });
  }
  
  deleteFood = (id) => {
    console.log("Delete dish!");
    //call api
  }
  
  componentDidMount() {
    Actions.getFoods(success => {
      //setup websocket connection
      // const ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
      // const ws = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host + "/ws/foods/");
      // this.foodWS = new ReconnectingWebSocket("ws://localhost:8000/ws/foods/");
      this.foodWS = new ReconnectingWebSocket("wss://reovofood.herokuapp.com/ws/foods/");
      this.foodWS.onopen = () => {
        console.log("Connection to foods/ open");
      };
      this.foodWS.onmessage = msg => {
        console.log("Message from foods/ received!");
        console.log("msg::: ", msg);
        const data = JSON.parse(msg.data);
        console.log("msg.data::: ", data);
        if(data.action === "create"){
          //new food was just created
          this.props.dispatch(appSetFoods([...this.props.foods.foods, data.object]));
        }
        else if(data.action === "update"){
          //food was just updated
          const list = [...this.props.foods.foods]; //copy current list of foods
          const food = list.filter(f => f.id === data.object.id)[0];
          if(food){
            list[list.indexOf(food)] = data.object;
            this.props.dispatch(appSetFoods(list));
          }
          else{
            //if food is undefined, just refetch the whole list of foods
            Actions.getFoods();
          }
        }
        else if(data.action === "delete"){
          //food was just deleted
          const list = [...this.props.foods.foods]; //copy current list of foods
          const food = list.filter(f => f.id === data.id)[0];
          if(food){
            list.splice(list.indexOf(food));
            this.props.dispatch(appSetFoods(list));
          }
          else{
            //if food is undefined, just refetch the whole list of foods
            Actions.getFoods();
          }
        }
      };
    });
    Actions.getCategories();
  }
  componentWillUnmount(){
    this.foodWS && this.foodWS.close();
  }
  render() {
    const data = [
      { title: 'Dish Title 1', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 2', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 3', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 4', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 5', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 6', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 7', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 8', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 9', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 10', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 11', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 12', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 13', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 14', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 15', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 16', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 17', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 18', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 19', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 20', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 21', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
      { title: 'Dish Title 22', img: imgs[Math.floor(Math.random() * (imgs.length + 0))] },
    ];

    let content;
    //if page is still loading data, show loading spinner
    if(this.props.foods.isFetching || this.props.categories.isFetching){
      content = (
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
            {/* <Spin size="large" /> */}
            <PizzaLoader />
        </Row>
      );
    }
    //if fetch failed
    else if(this.props.foods.isError || this.props.categories.isError){
      content = (
        <Row type="flex" justify="center" align="middle" style={{height: '40vh'}}>
          <Col md={12}>
            <Alert
              message="Connection Error"
              description="Please check your internet connection and reload this page."
              type="error"
              showIcon
            />
          </Col>
        </Row>
      );
    }
    else{
      //we have data
      content = (
      <>
        <Modal
          title="Edit Item"
          visible={this.state.editModalOpen}
          onOk={this.handleOk}
          onCancel={this.handleCancelEdit}
        >
          <EditFood />
        </Modal>
        <Row>
          <Col md={8} style={{ marginBottom: 20 }}>
            <Input.Search
              placeholder={`Search...`}
              onSearch={value => console.log(value)}
              enterButton
              onChange={(e) => this.setState({ search: e.target.value })}
            />
          </Col>
          <Col md={8} offset={1}>
            <Select style={{ width: '100%' }} placeholder={"Filter by Category"} onChange={this.onTypeFilterChanged}>
              <Select.Option key="-1" value="-1">All</Select.Option>
              {
                this.props.categories.categories.map(cat => (
                  <Select.Option key={cat.id} value={cat.id}>{cat.name}</Select.Option>
                ))
              }
            </Select>
          </Col>
        </Row>
        <List
          itemLayout="horizontal"
          dataSource={this.props.foods.foods}
          header={
            <Row type="flex" justify="end">
              <Tooltip title="Add new dish to menu">
                <Button type="primary" icon="plus" onClick={this.showCreateModal}>Add</Button>
              </Tooltip>
            </Row>
          }
          pagination={{
            // total: 500,
            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
            pageSize: 10,
            current: this.state.currentPage,
            showQuickJumper: true,
            hideOnSinglePage: true,
            onChange: this.onPageChanged,
          }}
          renderItem={item => (
            <List.Item actions={[
              <Popconfirm
                title="Are you sure you want to delete this dish?"
                onConfirm={() => this.deleteFood(item.id)}
                onCancel={() => console.log("Don't delete dish!")}
                okText="Yes"
                cancelText="No">
                <Button type="danger" icon="delete">Delete</Button>
              </Popconfirm>,
              <Tooltip title="Edit dish details">
                <Button type="primary" onClick={() => this.showEditModal(item)}>Edit</Button>
              </Tooltip>,
            ]}>
              <Row gutter={20}>
                <Col md={8}>
                  <img src={data[item.id].img} style={{ width: '100%' }} />
                </Col>
                <Col md={16}>
                  <h3>{item.name}</h3>
                  <p>{item.description}</p>
                </Col>
              </Row>
            </List.Item>
          )}
        />
      </>
    );
    }
    return (
      <>
        <h1 style={{textAlign: 'center', fontSize: '2em', marginBottom: 40}}>Food Menu</h1>
        {content}
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    foods: state.app.foods,
    categories: state.app.categories,
  };
}
export default connect(mapStateToProps)(FoodMenu);