import * as React from 'react';
import {Button, Row, Col, Spin,} from 'antd';
import { Moment } from 'moment';
import * as moment from 'moment';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';


interface Props{
  cropX: number;
  cropY: number;
  cropWidth: number;
  cropHeight: number;
  photo: any;
}
interface State{
  imgLoaded: boolean;
  imgWidth: number;
  imgHeight: number;
}
class AvatarDisplay extends React.Component<Props, State> {
  state = {
    imgLoaded: false,
    imgWidth: 0,
    imgHeight: 0,
  };

  componentDidMount() {
    const img = new Image();
    img.src = this.props.photo;
    img.onload = (e)=>{
      this.setState({
        imgLoaded: true,
        imgWidth: img.width,
        imgHeight: img.height,
      });
    };
  }

  componentDidUpdate(prevProps, prevState){
    if(prevProps.photo === this.props.photo){
      return;
    }
    this.setState({imgLoaded: false});
    const img = new Image();
    img.src = this.props.photo;
    img.onload = (e)=>{
      this.setState({
        imgLoaded: true,
        imgWidth: img.width,
        imgHeight: img.height,
      });
    };
  }
  
  render() {
    let t = this.props.cropY * this.state.imgHeight;
    let r = this.state.imgWidth - ((this.props.cropWidth + this.props.cropX) * this.state.imgWidth);
    let b = this.state.imgHeight - ((this.props.cropHeight + this.props.cropY) * this.state.imgHeight);
    let l = this.props.cropX * this.state.imgWidth;
    return (
      <div style={{
        position: 'relative',
        overflow: 'hidden',
        width: 35 * 4,
        height: 45 * 4,
      }}>
        <div style={{
          transformOrigin: 'top left',
          transform: `scale(${1 / ((this.props.cropWidth * this.state.imgWidth) / (35 * 4))})`,
        }}>
          {this.state.imgLoaded ? 
            <img
              src={this.props.photo}
              style={{
                position: 'absolute',
                minHeight: 45 * 4,
                minWidth: 35 * 4,
                // width: '100%',
                // height: '100%',
                clipPath: `inset(${t.toFixed(0)}px ${r.toFixed(0)}px ${b.toFixed(0)}px ${l.toFixed(0)}px)`,
                left: this.props.cropX * this.state.imgWidth * -1,
                top: this.props.cropY * this.state.imgHeight * -1,
              }} />
            :
              <Spin size="default" />
          }
        </div>
      </div>
    );
  }
}

export default connect()(AvatarDisplay);