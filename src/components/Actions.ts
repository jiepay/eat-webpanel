import {store} from '../index';
import axios from 'axios';
import {
  beginGetCustomers,
  failGetCustomers,
  appSetCustomers,
  beginGetShops,
  beginGetCategories,
  failGetCategories,
  appSetCategories,
  beginUpdateStoreDetails,
  failUpdateStoreDetails,
  appUpdateStoreDetails,
  failGetShops,
  appSetShops,
  beginGetSelfUser,
  failGetSelfUser,
  appSetSelfUser,
  beginLogin,
  failLogin,
  appLogin,
  logout,
  appSetIsInitializing,
  beginGetOrders,
  appSetOrders,
  failGetOrders,
  beginCreateAdmin,
  failCreateAdmin,
  appCreateAdmin,
  beginGetFoods,
  failGetFoods,
  appSetFoods,
  appUpdateOrderDelivered,
  beginUpdateOrderDelivered,
  failUpdateOrderDelivered,
  beginUpdateOrderCancelled,
  appUpdateOrderCancelled,
  failUpdateOrderCancelled} from '../actions/index';
import { Moment } from 'moment';

export default class Actions{
  
  static getCustomers = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetCustomers());
    axios.get('/api/customers/')
      .then((response) => {
        console.log(response);
        store.dispatch(appSetCustomers(response.data.results));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failGetCustomers(error));
        callback && callback(false);
      });
  }

  static getCategories = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetCategories());
    axios.get('/api/categories/')
      .then((response) => {
        console.log(response);
        store.dispatch(appSetCategories(response.data.results));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failGetCategories(error));
        callback && callback(false);
      });
  }
  
  static getShops = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetShops());
    axios.get('/api/shops/')
      .then((response) => {
        console.log(response);
        store.dispatch(appSetShops(response.data.results));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failGetShops(error));
        callback && callback(false);
      });
  }
  
  static getOrders = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetOrders());
    axios.get('/api/orders/')
      .then((response) => {
        console.log(response);
        store.dispatch(appSetOrders(response.data.results));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failGetOrders(error));
        callback && callback(false);
      });
  }
  
  static getFoods = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetFoods());
    axios.get('/api/foods/')
      .then((response) => {
        console.log(response);
        store.dispatch(appSetFoods(response.data.results));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failGetFoods(error));
        callback && callback(false);
      });
  }

  static updateStoreDetails = (storeName: string, address1: string, address2: string, longitude: number, latitude: number, opening: Moment, closing: Moment, openingdays: string, phone: number) => {
    //set is fetching
    store.dispatch(beginUpdateStoreDetails());
    axios.put('/api/shops/2/', {
      name: storeName,
      address1: address1,
      address2: address2,
      longitude: longitude,
      latitude: latitude,
      opening: opening.format("HH:mm"),
      open_sun: false,
      open_mon: false,
      open_tue: false,
      open_wed: false,
      open_thu: false,
      open_fri: false,
      open_sat: false,
      closing: closing.format("HH:mm"),
      open_public_hols: true,
      phone: phone,
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appUpdateStoreDetails(response.data));
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failUpdateStoreDetails(error));
      });
  }

  static updateOrderDelivered = (orderId: number) => {
    //set is fetching
    store.dispatch(beginUpdateOrderDelivered(orderId));
    axios.patch(`/api/orders/${orderId}/`, {
      status: 'DELIVERED',
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appUpdateOrderDelivered());
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failUpdateOrderDelivered(error));
      });
  }
  
  static updateOrderCancelled = (orderId: number) => {
    //set is fetching
    store.dispatch(beginUpdateOrderCancelled(orderId));
    axios.patch(`/api/orders/${orderId}/`, {
      status: 'CANCELLED',
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appUpdateOrderCancelled());
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failUpdateOrderCancelled(error));
      });
  }
  
  static login = (email: string, password: string) => {
    //set is fetching
    store.dispatch(beginLogin());
    axios.post('/api/login', {
      email: email,
      password: password,
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appSetIsInitializing(true));
        store.dispatch(appLogin(btoa(`${email}:${password}`), response.data));
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failLogin(error));
      });
  }

  static createAdmin = (values: any, callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginCreateAdmin());
    axios.post('/api/users', {
      first_name: values.first_name,
      last_name: values.last_name,
      email: values.email.trim().toLowerCase(),
      password: values.password,
      country: values.country.toLowerCase(),
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appCreateAdmin(response.data));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failCreateAdmin(error));
        callback && callback(false);
      });
  }
  
  static editAdmin = (id: number, values: any, callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginCreateAdmin());
    axios.put(`/api/users/${id}`, {
      first_name: values.first_name,
      last_name: values.last_name,
      email: values.email.trim().toLowerCase(),
      password: values.password,
      country: values.country.toLowerCase(),
    })
      .then((response) => {
        console.log(response);
        store.dispatch(appCreateAdmin(response.data));
        callback && callback(true);
      })
      .catch(function (error) {
        console.log(error);
        store.dispatch(failCreateAdmin(error));
        callback && callback(false);
      });
  }
    
  static getSelfUser = (callback?: (success: boolean)=>void) => {
    //set is fetching
    store.dispatch(beginGetSelfUser());
    axios.get('/api/me')
    .then((response) => {
      console.log(response);
      store.dispatch(appSetSelfUser(response.data));
      callback && callback(true);
    })
    .catch(function (error) {
      console.log(error);
      store.dispatch(failGetSelfUser(error));
      if(error.response && error.response.status === 401){
        store.dispatch(logout());
      }
      callback && callback(false);
    });
  }
}