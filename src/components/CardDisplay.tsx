// import * as React from 'react';
// import {Layout, Menu, Icon, Row, Col,} from 'antd';
// import { connect } from 'react-redux';
// import AvatarDisplay from './AvatarDisplay';

// const qrSample = require('../res/img/qr_sample.png');
// const muFlag = require('../res/img/mu.png');
// const { Header, Sider, Content, Footer } = Layout;

// interface Props{
//   // user: boolean;
//   app: any;
//   auth: any;
//   match?: any;
// }
// interface State{}

// class CardDisplay extends React.Component<Props, State> {
//   render() {
//     const app = this.props.app.applications.applications.filter(a => a.id === parseInt(this.props.match.params.id))[0];
//     const size = 5;
//     return (
//       <div className="Home">
//         <Row type="flex" justify="center" className="print-break-after">
//           <div style={{
//             border: '3px solid black',
//             borderRadius: 15,
//             height: 148 * size,
//             width: 105 * size,
//             padding: 15,
//             }}>
//             <Row style={{marginBottom: 20}}>
//               <Col span={8}>
//                 <AvatarDisplay
//                   photo={app.photo}
//                   cropX={app.photo_crop_x}
//                   cropY={app.photo_crop_y}
//                   cropWidth={app.photo_crop_width}
//                   cropHeight={app.photo_crop_height} />
//               </Col>
//               <Col span={8} style={{fontFamily: 'times'}}>
//                 <div style={{fontSize: '8.5em', color: 'blue', lineHeight: 1}}>10<sup>e</sup></div>
//                 <div style={{fontSize: '1em', fontWeight: 'bold'}}>Jeux des Iles</div>
//                 <div style={{fontSize: '1em', fontWeight: 'bold'}}>de l'Océan Indien</div>
//               </Col>
//             </Row>
//             <Row style={{marginBottom: 20}}>
//               <Col span={8}>
//                 <div style={{
//                     textTransform: 'uppercase',
//                     fontSize: '1.5em',
//                   }}>{app.last_name}</div>
//                 <div style={{
//                     fontSize: '1.3em',
//                   }}>{app.name}</div>
//               </Col>
//             </Row>
//             <Row style={{marginBottom: 20}}>
//               <Col span={8}>
//                 <div style={{
//                     textTransform: 'uppercase',
//                     color: '#006',
//                     fontSize: '1.4em',
//                   }}>{app.status}</div>
//                 <div style={{
//                   textTransform: 'uppercase',
//                   color: '#006',
//                   fontSize: '1.4em',
//                   }}>{countries.filter(c => c.code.toLowerCase() === app.country)[0].name || "Unknown Country"}</div>
//               </Col>
//               <Col span={8}>
//                 <img src={muFlag} style={{width: '70%'}} />
//               </Col>
//             </Row>
//             <Row style={{marginBottom: 20}}>
//               <Col span={8}>
//                 <div style={{
//                   }}>
//                   <QRCode value={`https://jioi.herokuapp.com/accreds/${app.id}/view`} />  
//                 </div>
//               </Col>
//               <Col span={8}>
//                 Facilities...
//               </Col>
//               <Col span={8}>
//                 <div style={{
//                   fontSize: '13em',
//                   position: 'absolute',
//                   top: -120,
//                 }}>
//                   A
//                 </div>
//               </Col>
//             </Row>
//             <Row>
//               <Col span={8}>
//                 <div style={{
                    
//                   }}>Partners...</div>
//               </Col>
//               <Col span={8}>
//                 Partners...
//               </Col>
//             </Row>
//           </div>
//         </Row>
//       </div>
//     );
//   }
// }


// export default connect()(CardDisplay);