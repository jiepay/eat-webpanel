import * as React from 'react';
import { Button, Layout, Menu, Spin, notification, Row, Col, Card, Tabs, Table, Modal, Input, Alert, Select, Tooltip, Popconfirm } from 'antd';
import { CalendarMode } from 'antd/lib/calendar';
import { Moment } from 'moment';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import ls from 'local-storage';

import SiderPageLayout from './SiderPageLayout';
import Utils from '../Utils';
import Actions from './Actions';
import AvatarDisplay from './AvatarDisplay';
import SiderMenu from './SiderMenu';
import Orders from './Orders';
import Customers from './Customers';
import { beginGetCustomers } from '../actions';

const { Header, Sider, Content, Footer } = Layout;

interface Props {
  type: "pending" | "delivered" | "cancelled";
  orders: any[];
  customers: any[];
}
interface State {
  activeOrder: any;
  modalOpen: boolean;
  modalEditable: boolean;
  search: string;
}
class OrdersTable extends React.Component<Props, State> {
  state = {
    activeOrder: null as any,
    modalOpen: false,
    modalEditable: false,
    search: "",
  };
  formRef;

  openNotificationWithIcon = (type: string, title: string, description: string) => {
    notification[type]({
      message: title,
      description: description,
    });
  };

  approve = (id: number) => {
    // Actions.approveApplication(id);
    this.setState({ modalOpen: false, activeOrder: null });
  }

  render() {
    const filteredOrders = this.props.orders.filter((app: any) => {
      const search = this.state.search.trim().toLowerCase();
      const cust = this.props.customers.filter((c, index) => c.id === app.customer)[0];
      let pass = true;
      if (search.length > 0) {
        //we have search term
        pass = false;
        const a = app;
        let prices = 0;
        app.entries.forEach(e => prices += e.price * e.quantity);
        const mash: string[] = [
          a.id,
          cust.first_name,
          cust.last_name,
          prices,
          moment(a.date_ordered).format("lll"),
          moment(a.date_delivered).format("lll"),
        ];
        if (mash.join(" ").toLowerCase().indexOf(search) != -1) {
          pass = true;
        }
      }
      return pass;
    });
    const orders = filteredOrders.map((ord: any, index: number) => {
      const customer = this.props.customers.filter((c, index) => c.id === ord.customer)[0];
      let price = 0;
      ord.entries.forEach(e => price += e.price * e.quantity);
      return {
        key: ord.id,
        customer: customer.first_name + " " + customer.last_name,
        date_ordered: moment(ord.date_ordered).format("lll"),
        price: price,
        date_delivered: moment(ord.date_delivered).format("lll"),
      };
    });
    return (
      <>
      <Row>
        <Col md={8} style={{ marginBottom: 20 }}>
          <Input.Search
            placeholder={`Search...`}
            onSearch={value => console.log(value)}
            enterButton
            onChange={(e) => this.setState({ search: e.target.value })}
          />
        </Col>
        {/* <Col md={8} offset={1}>
            <Select style={{ width: '100%' }} placeholder={translations(l).filterByType} onChange={this.onTypeFilterChanged}>
              <Select.Option key="-1" value="">All</Select.Option>
              <Select.Option key={0} value={status}>Blah</Select.Option>
            </Select>
          </Col> */}
      </Row>
      <Table
        dataSource={orders}
        onRow={(record: any) => {
          return {
            onClick: () => {
              console.log(record);
              const app = this.props.orders.filter(a => a.id === record.key)[0];
              this.setState({ activeOrder: app, modalOpen: true, modalEditable: true });
            },
          };
        }}
        rowClassName={(record, index) => "cursor-pointer"} >
        <Table.Column
          title={"ID"}
          dataIndex="key"
          key="id"
        />
        <Table.Column
          title={"Customer"}
          dataIndex="customer"
          key="customer"
        />
        <Table.Column
          title={"Date Ordered"}
          dataIndex="date_ordered"
          key="date_ordered"
        />
        <Table.Column
          title={"Price"}
          dataIndex="price"
          key="price"
        />
        {
          this.props.type === "delivered" &&
          <Table.Column
            title={"Date Delivered"}
            dataIndex="date_delivered"
            key="date_delivered"
          />
        }
        {
          this.props.type === "pending" &&
          <Table.Column
            title={"Delivered"}
            dataIndex="delivered"
            //key="delivered"
            render={(text, record: any, index) =>
              <Popconfirm
                title="Are you sure you want to mark this ordered as delivered?"
                onConfirm={() => {
                  const order = this.props.orders.filter(o => o.id == record.key)[0];
                  Actions.updateOrderDelivered(order.id);

                  notification.success({
                    message: 'Eat Food Hungry',
                    description: 'Order has been delivered',
                    style: {
                      width: 600,
                      marginLeft: 335 - 600,
                    },
                  });
                }}
                onCancel={() => console.log("Don't deliver!")}
                okText="Yes"
                cancelText="No">
                <Button type="primary">Mark as Delivered</Button>
              </Popconfirm>
            }
          />
        }
        {
          this.props.type === "pending" &&
          <Table.Column
            title={"Cancelled"}
            // dataIndex="cancelled"
            key="cancelled"
            render={(text, record: any, index) =>
              <Popconfirm
                title="Are you sure you want to mark this ordered as cancelled?"
                onConfirm={() => {
                  const order = this.props.orders.filter(o => o.id == record.key)[0];
                  Actions.updateOrderCancelled(order.id);

                  notification.warning({
                    message: 'Eat Food Hungry',
                    description: 'Order has been cancelled',
                    style: {
                      width: 600,
                      marginLeft: 335 - 600,
                    },
                  });
                }}
                onCancel={() => console.log("Don't cancel!")}
                okText="Yes"
                cancelText="No">
                <Button type="danger">Mark as Cancelled</Button>
              </Popconfirm>
            }
          />
        }
      </Table>
      </>
    );
  }
}

export default connect()(OrdersTable);