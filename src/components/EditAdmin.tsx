// import * as React from 'react';
// import { Row, Col, Card, Upload, Button, Icon, Table, Spin, Tooltip, } from 'antd';
// import { Link } from 'react-router-dom';
// import * as XLSX from 'xlsx';
// import PageLayout from './PageLayout';
// import CreateAdminForm from './CreateAdminForm';
// import Actions from './Actions';

// interface Props{
//   auth: any;
//   app: any;
//   match?: any;
// }
// interface State{}

// export default class EditAdmin extends React.Component<Props, State> {
//   state = {}
//   render() {
//     const id = parseInt(this.props.match.params.id);
//     const admin = this.props.app.users.users.filter((user, index) => user.id === id)[0];
//     console.log("admin: ");
//     console.log(this.props.match.params.id);
//     console.log(this.props.app.users.users);
//     console.log(this.props.app.users.users[2]);
//     console.log(admin);
//     return (
//       <PageLayout>
//         <Row type="flex" justify="space-around" align="top" style={{flex: '1 1 auto'}}>
//           <Col md={16}>
//             <div style={{height: 30}}></div>
//             <Card hoverable={false} title={<Row><h3 style={{textAlign: 'center'}}>{translations(this.props.app.locale).editAdmin}</h3></Row>}>
//               <Row type="flex">
//                 <Col md={4}>
//                   <Link to="/">
//                     <Button icon="left">Go Back</Button>
//                   </Link>
//                 </Col>
//               </Row>
//               <CreateAdminForm locale={this.props.app.locale} auth={this.props.auth} app={this.props.app} admin={admin} />
//             </Card>
//           </Col>
//         </Row>
//       </PageLayout>
//     );
//   }
// }
