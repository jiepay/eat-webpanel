// import * as React from 'react';
// import {Button, Layout, Menu, Spin, notification, Row, Col, Card, message} from 'antd';
// import { CalendarMode } from 'antd/lib/calendar';
// import { Moment } from 'moment';
// import * as moment from 'moment';
// import { connect } from 'react-redux';
// import {Link} from 'react-router-dom';

// import ApplicationForm from './ApplicationForm';
// import PageLayout from './PageLayout';
// import Actions from './Actions';
// import CardDisplay from './CardDisplay';
// import AvatarDisplay from './AvatarDisplay';
// import Utils from '../Utils';

// const { Header, Sider, Content, Footer } = Layout;

// interface Props{
//   app: any; //entire app state
//   auth: any;
//   dispatch?: any;
//   match?: any;
// }
// interface State{}
// class ViewApplication extends React.Component<Props, State> {
//   state = {};
  
//   render() {
//     const l = this.props.app.locale;
//     const app = this.props.app.applications.applications.filter(a => a.id === parseInt(this.props.match.params.id))[0];
//     let disc;
//     if(app){
//       disc = app.discipline.split(",");
//       disc = disc.map((d) => Utils.capitalize(d));
//     }
//     return (
//       <PageLayout>
//         <Row type="flex" justify="space-around" style={{width: '100%'}}>
//           <Col md={16}>
//             <Card style={{backgroundColor: '#fff'}}>
//               <h1 style={{textAlign: 'center', fontSize: '2em', marginBottom: 40}}>View Order</h1>
//               <p><span style={{fontWeight: 'bold'}}>Last Name:</span> <span>{app.last_name}</span></p>
//               <p><span style={{fontWeight: 'bold'}}>First Name:</span> <span>{app.name}</span></p>
//             </Card>
//           </Col>
//         </Row>
//       </PageLayout>
//     );
//   }
// }

// export default connect()(ViewApplication);