import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import './res/app.less';

import { Provider } from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import jioiApp from './reducers';
import logger from 'redux-logger';
// import thunkMiddleware from 'redux-thunk';
import Utils from './Utils';


const DATA = {};
const store = createStore(
  jioiApp, 
  DATA,
  applyMiddleware(
    // thunkMiddleware,
    logger,
  ),
);
ReactDOM.render(
  <Provider store={store}>
    <App store={store} />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
//registerServiceWorker();

export {store};