import * as ls from "local-storage";
import {
  AppSetAccountTypeAction,
  APP_SET_ACCOUNT_TYPE,
  AppSetLocaleAction,
  APP_SET_LOCALE,
  AppSetCustomersAction,
  APP_SET_CUSTOMERS,
  AppSetShopsAction,
  APP_SET_SHOPS,
  BEGIN_GET_CUSTOMERS,
  BEGIN_GET_SHOPS,
  BeginGetCustomersAction,
  BeginGetShopsAction,
  BeginGetCategoriesAction,
  FailGetCategoriesAction,
  BEGIN_UPDATE_STORE_DETAILS,
  FAIL_UPDATE_STORE_DETAILS,
  APP_SET_UPDATE_STORE_DETAILS,
  AppSetCategoriesAction,
  BEGIN_GET_CATEGORIES,
  FAIL_GET_CATEGORIES,
  APP_SET_CATEGORIES,
  FailGetShopsAction,
  FailGetCustomersAction,
  FAIL_GET_SHOPS,
  FAIL_GET_CUSTOMERS,
  FAIL_CREATE_APPLICATION,
  BEGIN_CREATE_APPLICATION,
  APP_CREATE_APPLICATION,
  BeginCreateApplicationAction,
  FailCreateApplicationAction,
  AppCreateApplicationAction,
  BeginCreateAdminAction,
  FailCreateAdminAction,
  AppCreateAdminAction,
  BEGIN_CREATE_ADMIN,
  FAIL_CREATE_ADMIN,
  APP_CREATE_ADMIN,
  BeginGetOrdersAction,
  FailGetOrdersAction,
  AppSetOrdersAction,
  APP_SET_ORDERS,
  FAIL_GET_ORDERS,
  BEGIN_GET_ORDERS,
  BeginGetFoodsAction,
  FailGetFoodsAction,
  AppSetFoodsAction,
  BEGIN_GET_FOODS,
  FAIL_GET_FOODS,
  APP_SET_FOODS,
  BeginUpdateStoreDetailsAction,
  FailUpdateStoreDetailsAction,
  AppUpdateStoreDetailsAction,
  BeginUpdateOrderDeliveredAction,
  FailUpdateOrderDeliveredAction,
  AppUpdateOrderDeliveredAction,
  BEGIN_UPDATE_ORDER_DELIVERED,
  FAIL_UPDATE_ORDER_DELIVERED,
  APP_SET_UPDATE_ORDER_DELIVERED,
} from "../actions";

const setLocale = (appState: any, action: AppSetLocaleAction) => {
  ls.set("locale", action.locale);
  return {
    ...appState,
    locale: action.locale,
  }
}

const beginGetCustomers = (appState: any, action: BeginGetCustomersAction) => {
  return {
    ...appState,
    customers: {
      ...appState.customers,
      isFetching: true,
      isError: false,
      error: null,
    }
  }
}

const failGetCustomers = (appState: any, action: FailGetCustomersAction) => {
  return {
    ...appState,
    customers: {
      ...appState.customers,
      isFetching: false,
      isError: true,
      error: action.error,
    }
  }
}

const setCustomers = (appState: any, action: AppSetCustomersAction) => {
  // ls.set("users", action.users);
  return {
    ...appState,
    customers: {
      ...appState.customers,
      isFetching: false,
      isError: false,
      error: null,
      customers: action.customers,
    }
  }
}

const beginGetFoods = (appState: any, action: BeginGetFoodsAction) => {
  return {
    ...appState,
    foods: {
      ...appState.foods,
      isFetching: true,
      isError: false,
      error: null,
    }
  }
}

const failGetFoods = (appState: any, action: FailGetFoodsAction) => {
  return {
    ...appState,
    foods: {
      ...appState.foods,
      isFetching: false,
      isError: true,
      error: action.error,
    }
  }
}

const setFoods = (appState: any, action: AppSetFoodsAction) => {
  // ls.set("foods", action.foods);
  return {
    ...appState,
    foods: {
      ...appState.foods,
      isFetching: false,
      isError: false,
      error: null,
      foods: action.foods,
    }
  }
}

const beginGetCategories = (appState: any, action: BeginGetCategoriesAction) => {
  return {
    ...appState,
    categories: {
      ...appState.categories,
      isFetching: true,
      isError: false,
      error: null,
    }
  }
}

const failGetCategories = (appState: any, action: FailGetCategoriesAction) => {
  return {
    ...appState,
    categories: {
      ...appState.categories,
      isFetching: false,
      isError: true,
      error: action.error,
    }
  }
}

const setCategories = (appState: any, action: AppSetCategoriesAction) => {
  // ls.set("categories", action.categories);
  return {
    ...appState,
    categories: {
      ...appState.categories,
      isFetching: false,
      isError: false,
      error: null,
      categories: action.categories,
    }
  }
}

const beginGetShops = (appState: any, action: BeginGetShopsAction) => {
  return {
    ...appState,
    shops: {
      ...appState.shops,
      isFetching: true,
      isError: false,
      error: null,
    }
  }
}

const failGetShops = (appState: any, action: FailGetShopsAction) => {
  return {
    ...appState,
    shops: {
      ...appState.shops,
      isFetching: false,
      isError: true,
      error: action.error,
    }
  }
}

const setShops = (appState: any, action: AppSetShopsAction) => {
  // ls.set("shops", action.shops);
  return {
    ...appState,
    shops: {
      ...appState.shops,
      isFetching: false,
      isError: false,
      error: null,
      shops: action.shops,
    }
  }
}

const beginGetOrders = (appState: any, action: BeginGetOrdersAction) => {
  return {
    ...appState,
    orders: {
      ...appState.orders,
      isFetching: true,
      isError: false,
      error: null,
    }
  }
}

const failGetOrders = (appState: any, action: FailGetOrdersAction) => {
  return {
    ...appState,
    orders: {
      ...appState.orders,
      isFetching: false,
      isError: true,
      error: action.error,
    }
  }
}

const setOrders = (appState: any, action: AppSetOrdersAction) => {
  // ls.set("orders", action.orders);
  return {
    ...appState,
    orders: {
      ...appState.orders,
      isFetching: false,
      isError: false,
      error: null,
      orders: action.orders,
    }
  }
}

const beginCreateApplication = (appState: any, action: BeginCreateApplicationAction) => {
  return {
    ...appState,
    applications: {
      ...appState.applications,
      create: {
        ...appState.applications.create,
        isFetching: true,
        isError: false,
        error: null,
      }
    }
  }
}

const failCreateApplication = (appState: any, action: FailCreateApplicationAction) => {
  return {
    ...appState,
    applications: {
      ...appState.applications,
      create: {
        ...appState.applications.create,
        isFetching: false,
        isError: true,
        error: action.error,
      }
    }
  }
}

const createApplication = (appState: any, action: AppCreateApplicationAction) => {
  return {
    ...appState,
    applications: {
      ...appState.applications,
      create: {
        ...appState.applications.create,
        isFetching: false,
        isError: false,
        error: null,
      }
    }
  }
}

const beginUpdateOrderDelivered = (appState: any, action: BeginUpdateOrderDeliveredAction) => {
  return {
    ...appState,
    orders: {
      ...appState.orders,
      create: {
        ...appState.orders.create,
        isFetching: true,
        isError: false,
        error: null,
      }
    }
  }
}

const failUpdateOrderDelivered = (appState: any, action: FailUpdateOrderDeliveredAction) => {
  return {
    ...appState,
    orders: {
      ...appState.orders,
      create: {
        ...appState.orders.create,
        isFetching: false,
        isError: true,
        error: action.error,
      }
    }
  }
}

const updateOrderDelivered = (appState: any, action: AppUpdateOrderDeliveredAction) => {
  return {
    ...appState,
    orders: {
      ...appState.orders,
      create: {
        ...appState.orders.create,
        isFetching: false,
        isError: false,
        error: null,
      }
    }
  }
}

const beginUpdateStoreDetails = (appState: any, action: BeginUpdateStoreDetailsAction) => {
  return {
    ...appState,
    shops: {
      ...appState.shops,
      create: {
        ...appState.shops.create,
        isFetching: true,
        isError: false,
        error: null,
      }
    }
  }
}

const failUpdateStoreDetails = (appState: any, action: FailUpdateStoreDetailsAction) => {
  return {
    ...appState,
    shops: {
      ...appState.shops,
      create: {
        ...appState.shops.create,
        isFetching: false,
        isError: true,
        error: action.error,
      }
    }
  }
}

const updateStoreDetails = (appState: any, action: AppUpdateStoreDetailsAction) => {
  return {
    ...appState,
    shops: {
      ...appState.shops,
      create: {
        ...appState.shops.create,
        isFetching: false,
        isError: false,
        error: null,
      }
    }
  }
}

const beginCreateUser = (appState: any, action: BeginCreateAdminAction) => {
  return {
    ...appState,
    users: {
      ...appState.users,
      create: {
        ...appState.users.create,
        isFetching: true,
        isError: false,
        error: null,
      }
    }
  }
}

const failCreateUser = (appState: any, action: FailCreateAdminAction) => {
  return {
    ...appState,
    users: {
      ...appState.users,
      create: {
        ...appState.users.create,
        isFetching: false,
        isError: true,
        error: action.error,
      }
    }
  }
}

const createUser = (appState: any, action: AppCreateAdminAction) => {
  return {
    ...appState,
    users: {
      ...appState.users,
      create: {
        ...appState.users.create,
        isFetching: false,
        isError: false,
        error: null,
      }
    }
  }
}

const defaultState = {
  locale: ls.get("locale") || "en",
  customers: {
    isFetching: false,
    isError: false,
    error: null,
    customers: ls.get("customers") || [],
    create: {
      isFetching: false,
      isError: false,
      error: null,
    }
  },
  orders: {
    isFetching: false,
    isError: false,
    error: null,
    orders: ls.get("orders") || [],
    create: {
      isFetching: false,
      isError: false,
      error: null,
    }
  },
  shops: {
    isFetching: false,
    isError: false,
    error: null,
    shops: ls.get("shops") || [],
    create: {
      isFetching: false,
      isError: false,
      error: null,
    }
  },
  foods: {
    isFetching: false,
    isError: false,
    error: null,
    foods: ls.get("foods") || [],
    create: {
      isFetching: false,
      isError: false,
      error: null,
    }
  },
  categories: {
    isFetching: false,
    isError: false,
    error: null,
    categories: ls.get("categories") || [],
    create: {
      isFetching: false,
      isError: false,
      error: null,
    }
  },
};

//default export
const app = (appState: any = defaultState, action: any) => {
  switch (action.type) {
    case APP_SET_LOCALE:
      return setLocale(appState, action);
    case BEGIN_GET_CUSTOMERS:
      return beginGetCustomers(appState, action);
    case BEGIN_GET_FOODS:
      return beginGetFoods(appState, action);
    case FAIL_GET_FOODS:
      return failGetFoods(appState, action);
    case APP_SET_FOODS:
      return setFoods(appState, action);
    case BEGIN_GET_SHOPS:
      return beginGetShops(appState, action);
    case FAIL_GET_SHOPS:
      return failGetShops(appState, action);
    case APP_SET_SHOPS:
      return setShops(appState, action);
    case BEGIN_GET_CATEGORIES:
      return beginGetCategories(appState, action);
    case FAIL_GET_CATEGORIES:
      return failGetCategories(appState, action);
    case APP_SET_CATEGORIES:
      return setCategories(appState, action);
    case BEGIN_GET_ORDERS:
      return beginGetOrders(appState, action);
    case FAIL_GET_ORDERS:
      return failGetOrders(appState, action);
    case APP_SET_ORDERS:
      return setOrders(appState, action);
    case FAIL_GET_CUSTOMERS:
      return failGetCustomers(appState, action);
    case APP_SET_CUSTOMERS:
      return setCustomers(appState, action);
    case BEGIN_CREATE_APPLICATION:
      return beginCreateApplication(appState, action);
    case FAIL_CREATE_APPLICATION:
      return failCreateApplication(appState, action);
    case APP_CREATE_APPLICATION:
      return createApplication(appState, action);
    case BEGIN_CREATE_ADMIN:
      return beginCreateUser(appState, action);
    case FAIL_CREATE_ADMIN:
      return failCreateUser(appState, action);
    case APP_CREATE_ADMIN:
      return createUser(appState, action);
    case BEGIN_UPDATE_STORE_DETAILS:
      return beginUpdateStoreDetails(appState, action);
    case FAIL_UPDATE_STORE_DETAILS:
      return failUpdateStoreDetails(appState, action);
    case APP_SET_UPDATE_STORE_DETAILS:
      return updateStoreDetails(appState, action);
    case BEGIN_UPDATE_ORDER_DELIVERED:
      return beginUpdateOrderDelivered(appState, action);
    case FAIL_UPDATE_ORDER_DELIVERED:
      return failUpdateOrderDelivered(appState, action);
    case APP_SET_UPDATE_ORDER_DELIVERED:
      return updateOrderDelivered(appState, action);
    default:
      return appState
  }
}

export default app