import * as ls from "local-storage";
import axios from 'axios';
import {
  LOGIN, 
  LOGOUT,
  BEGIN_GET_SELF_USER,
  FAIL_GET_SELF_USER,
  APP_SET_SELF_USER,
  BeginGetSelfUserAction,
  FailGetSelfUserAction,
  AppSetSelfUserAction,
  BEGIN_LOGIN,
  FAIL_LOGIN,
  APP_LOGIN,
  AppSetIsInitializingAction,
  APP_SET_IS_INITIALIZING,
} from "../actions";

const beginLogin = (authState: any, action: any) => {
  return {
    ...authState,
    isFetching: true,
    isError: false,
    error: null,
    token: null, // the API response body
    authenticated: false,
    user: null,
  }
}

const failLogin = (authState: any, action: any) => {
  return {
    ...authState,
    isInitializing: false,
    isFetching: false,
    isError: true,
    error: action.error,
    token: null,
    authenticated: false,
    user: null,
  }
}

const appLogin = (authState: any, action: any) => {
  ls.set("token", action.token);
  ls.set("user", action.user);
  axios.defaults.headers.common['Authorization'] = `Basic ${action.token}`;
  return {
    ...authState,
    isInitializing: false,
    isFetching: false,
    isError: false,
    error: null,
    token: action.token,
    authenticated: true,
    user: action.user,
  }
}

const logout = (authState: any, action: any) => {
  ls.remove("token");
  ls.remove("user");
  delete axios.defaults.headers.common["Authorization"];
  return {
    ...authState,
    isFetching: false,
    isError: false,
    error: null,
    token: null,
    authenticated: false,
    user: null,
  }
}

const beginGetSelfUser = (authState: any, action: BeginGetSelfUserAction) => {
  return {
    ...authState,
    isFetching: true,
    isError: false,
    error: null,
  }
}

const failGetSelfUser = (authState: any, action: FailGetSelfUserAction) => {
  return {
    ...authState,
    isInitializing: false,
    isFetching: false,
    isError: true,
    error: action.error,
  }
}

const setSelfUser = (authState: any, action: AppSetSelfUserAction) => {
  return {
    ...authState,
    isInitializing: false,
    isFetching: false,
    isError: false,
    error: null,
    user: action.user,
    authenticated: true,
  }
}

const setIsInitializing = (authState: any, action: AppSetIsInitializingAction) => {
  return {
    ...authState,
    isInitializing: action.isInitializing,
  }
}

const defaultState = {
  isInitializing: true,
  isFetching: false,
  isError: false,
  error: null,
  token: ls.get("token") || null,
  authenticated: ls.get("token") != null && ls.get("user") != null,
  user: ls.get("user") || null,
};
const auth = (authState = defaultState, action: any) => {
  switch (action.type) {
    case APP_SET_IS_INITIALIZING:
      return setIsInitializing(authState, action);
    case BEGIN_LOGIN:
      return beginLogin(authState, action);
    case FAIL_LOGIN:
      return failLogin(authState, action);
    case APP_LOGIN:
      return appLogin(authState, action);
    case LOGOUT:
      return logout(authState, action);
    case BEGIN_GET_SELF_USER:
      return beginGetSelfUser(authState, action);
    case FAIL_GET_SELF_USER:
      return failGetSelfUser(authState, action);
    case APP_SET_SELF_USER:
      return setSelfUser(authState, action);

    default:
      return authState
  }
}

export default auth