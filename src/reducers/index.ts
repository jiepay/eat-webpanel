import { combineReducers } from 'redux';
import auth from './auth';
import app from './app';

const jioiApp = combineReducers({
  auth,
  app,
});

export default jioiApp;