from os import listdir
from os.path import isfile, join

path = "build/static/js/"

filepath = [join(path, f) for f in listdir(path)
        if isfile(join(path, f))
        and f.startswith("main.")
        and f.endswith(".js")][0]

content = ""
# when using with, file is automatically closed
with open(filepath, mode='r') as f:
  content = f.read()
with open(filepath, 'w') as f:
  content = content.replace("/static/main/service-worker.js", "/service-worker.js")
  f.write(content)
  print("Service worker path updated in main.js")
